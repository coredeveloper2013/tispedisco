<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketReply extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ticket_id', 'user_id', 'message', 'file', 'status', 'user_type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'user_id');
    }

    public function replies()
    {
        return $this->hasMany(InnerReply::class, 'reply_id');
    }

    public function scopeCreator($query)
    {
        return $query
            ->when($this->user_type == 'user',function($q){
                return $q->with('user')->get();
            })
            ->when($this->user_type == 'admin',function($q){
                return $q->with('admin');
            })->get();
    }
}
