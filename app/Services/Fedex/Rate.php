<?php

namespace App\Services\Fedex;


use App\Carrier;
use Carbon\Carbon;
use FedEx\RateService\Request as FedExRequest;
use FedEx\RateService\ComplexType;
use FedEx\RateService\SimpleType;

class Rate
{
	public static function getServices($config, $orderData)
	{
		if ($orderData == null) {
    		return [
    			'status' => false,
    			'message' => 'No order data found for FedEx API.'
    		];
    	}

	    $rateRequest = new ComplexType\RateRequest();
    	$rateRequest->WebAuthenticationDetail->UserCredential->Key = $config->app_key;
		$rateRequest->WebAuthenticationDetail->UserCredential->Password = $config->password;
		$rateRequest->ClientDetail->AccountNumber = $config->account_number;
		$rateRequest->ClientDetail->MeterNumber = $config->meter_number;

    	// Followed documentation
    	// PDF1: https://www.fedex.com/templates/components/apps/wpor/secure/downloads/pdf/201607/FedEx_WebServices_RateServices_WSDLGuide_v2016.pdf
    	// PDF2: https://www.fedex.com/us/developer/downloads/pdf/2018/FedEx_WebServices_DevelopersGuide_v2018.pdf
    	// https://www.fedex.com/ratefinder/standalone?method=getQuickQuote


		$rateRequest->TransactionDetail->CustomerTransactionId = 'testing rate service request';

		//version
		$rateRequest->Version->ServiceId    = 'crs';
		$rateRequest->Version->Major        = 24;
		$rateRequest->Version->Minor        = 0;
		$rateRequest->Version->Intermediate = 0;

		$rateRequest->ReturnTransitAndCommit = true;

		/** Shipper: Required Values
		 *     PostalCode
         *     CountryCode
         *     Address/StreetLines
         *     Address/City
         *     Address/StateOrProvinceCode
		 */

		$rateRequest->RequestedShipment->PreferredCurrency                     = 'EUR';
		$rateRequest->RequestedShipment->Shipper->Address->StreetLines         = explode(',', $orderData['fromAddressLine']);
		$rateRequest->RequestedShipment->Shipper->Address->City                = $orderData['fromCity'] ?? '';
		$rateRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = $orderData['fromStateOrProvinceCode'] ?? '';
		$rateRequest->RequestedShipment->Shipper->Address->PostalCode          = $orderData['fromPostalCode'] ?? '';
		$rateRequest->RequestedShipment->Shipper->Address->CountryCode         = $orderData['fromCountryCode'];

		/** Recipient: Required Values
		 *     PostalCode
         *     CountryCode
         *     Address
		 */
		$rateRequest->RequestedShipment->Recipient->Address->StreetLines         = explode(',', $orderData['toAddressLine']); // Takes array of street lines
		$rateRequest->RequestedShipment->Recipient->Address->City                = $orderData['toCity'] ?? '';
		$rateRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = $orderData['toStateOrProvinceCode'] ?? '';
		$rateRequest->RequestedShipment->Recipient->Address->PostalCode          = $orderData['toPostalCode'] ?? '';
		$rateRequest->RequestedShipment->Recipient->Address->CountryCode         = $orderData['toCountryCode'] ?? '';


		// PDF2: Page 245
		//shipping charges payment
		$rateRequest->RequestedShipment->ShippingChargesPayment->PaymentType = SimpleType\PaymentType::_SENDER;

		//rate request types
		$rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_PREFERRED, SimpleType\RateRequestType::_LIST, SimpleType\RateRequestType::_NONE];

		$rateRequest->RequestedShipment->PackageCount = $orderData['packages'] ? count($orderData['packages']) : 1;

		// For insurance
		// $rateRequest->RequestedShipment->TotalInsuredValue = 1;
        $rateRequest->RequestedShipment->RequestedPackageLineItems = [];

        //create package line items
        foreach($orderData['packages'] as $key => $package){
            array_push($rateRequest->RequestedShipment->RequestedPackageLineItems, new ComplexType\RequestedPackageLineItem());

            // Look at PDF2, 197 Page
            $rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Value = $package['weight'] ?? 1;
            $rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Units = SimpleType\WeightUnits::_KG;

            // $rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Length = $package['length'] ?? 1;
            // $rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Width = $package['width'] ?? 1;
            // $rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Height =$package['height'] ?? 1;
            $rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Units = SimpleType\LinearUnits::_IN;

            $rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->GroupPackageCount = 1;
        }



		$rateServiceRequest = new FedExRequest();

		if (config('env') == 'production') {
			$rateServiceRequest->getSoapClient()->__setLocation(FedExRequest::PRODUCTION_URL);
		}

		$rateReply = $rateServiceRequest->getGetRatesReply($rateRequest)->toArray();

		$services = [];
    	$messages = '';

    	if ($rateReply['HighestSeverity'] == 'ERROR' || $rateReply['HighestSeverity'] == 'FAULT') {
    		return [
				'status'  => false,
				'message' => $rateReply['Notifications'][0]['Message']
    		];
    	}


    	if ($rateReply['HighestSeverity'] == 'WARNING') {
	    	// Extract response meessage
	    	if ($rateReply['Notifications']) {
	    		foreach ($rateReply['Notifications'] as $notification) {
	    			if ($notification['Severity'] == 'WARNING') {
		    			$messages .= $notification['Message'];
	    			}
	    		}
	    	}
    	}


		if (isset($rateReply['RateReplyDetails'])) {
    		$services = $rateReply['RateReplyDetails'];
    	}


    	return [
			'status'  => true,
			'data'    => $services,
			'message' => $messages
    	];
    }

    public static function getAccurateCost($config, $orderData)
    {
        if ($orderData == null) {
    		return [
    			'status' => false,
    			'message' => 'No order data found for FedEx API.'
    		];
    	}
        $services = self::getServices($config, $orderData);

        if($services['status'] == true){
            foreach($services['data'] as $service){
                if($service['ServiceType'] == $orderData['ServiceType']){
                    foreach($service['RatedShipmentDetails'] as $rate){
                        if($rate['ShipmentRateDetail']['RateType'] == $orderData['RateType']){
                            return [
                                'status' => true,
                                'CarrierSurcharges' => $rate['ShipmentRateDetail']['TotalSurcharges']['Amount'],
                                'cost' => $rate['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount']
                            ];
                        }
                    }
                }
            }
        } else {
            return [
                'status'  => false,
                'message' => $services['message']
            ];
        }
    }
}
