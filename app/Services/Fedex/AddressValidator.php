<?php 

namespace App\Services\Fedex;

use FedEx\AddressValidationService\Request;
use FedEx\AddressValidationService\ComplexType;
use FedEx\AddressValidationService\SimpleType;

class AddressValidator
{

	public static function validateAddress($config, $address)
	{
		$addressValidationRequest = new ComplexType\AddressValidationRequest();
		$addressValidationRequest->WebAuthenticationDetail->UserCredential->Key = $config->app_key;
		$addressValidationRequest->WebAuthenticationDetail->UserCredential->Password = $config->password;
		$addressValidationRequest->ClientDetail->AccountNumber = $config->account_number;
		$addressValidationRequest->ClientDetail->MeterNumber = $config->meter_number;

		// Version
		$addressValidationRequest->Version->ServiceId = 'aval';
		$addressValidationRequest->Version->Major = 4;
		$addressValidationRequest->Version->Intermediate = 0;
		$addressValidationRequest->Version->Minor = 0;

		// Address(es) to validate.
		$addressValidationRequest->AddressesToValidate = [new ComplexType\AddressToValidate()]; 
		$addressValidationRequest->AddressesToValidate[0]->Address->StreetLines = [$address['StreetLines']];
		$addressValidationRequest->AddressesToValidate[0]->Address->City = $address['City'] ?? '';
		$addressValidationRequest->AddressesToValidate[0]->Address->StateOrProvinceCode = $address['StateOrProvinceCode'] ?? '';
		$addressValidationRequest->AddressesToValidate[0]->Address->PostalCode = $address['PostalCode'] ?? '';
		$addressValidationRequest->AddressesToValidate[0]->Address->CountryCode = $address['CountryCode'] ?? '';

		$request = new Request();

		$addressValidationReply = $request->getAddressValidationReply($addressValidationRequest);

		return $addressValidationReply->toArray();
	}

}