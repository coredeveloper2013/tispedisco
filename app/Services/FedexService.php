<?php

namespace App\Services;

use App\Carrier;
use App\Services\Fedex\Rate;
use App\Services\Fedex\Shipment;
use App\Services\Fedex\AddressValidator;

class FedexService
{
	public static function getFedexConfig()
	{
    	$carrier = Carrier::where('title', 'Fedex')->first();

        return json_decode($carrier->configs);
	}

	public static function getServices($orderData)
	{
		return Rate::getServices(self::getFedexConfig(), $orderData);
	}

    public static function getAccurateCost($orderData)
    {
        return Rate::getAccurateCost(self::getFedexConfig(), $orderData);
    }

	public static function validateAddress($address)
	{
		return AddressValidator::validateAddress(self::getFedexConfig(), $address);
	}

	public static function createShipingRequest($shippingData)
	{
    	return Shipment::createShipment(self::getFedexConfig(), $shippingData);
	}
}
