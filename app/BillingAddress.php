<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    protected $guarded = ['id'];
    public function user()
    {
        $this->belongsTo(User::class);
    }
}
