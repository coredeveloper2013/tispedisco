<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Config;
use Session;

class LangMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Session::has('appLocale') && array_key_exists(Session::get('appLocale'), Config::get('languages'))) {
            App::setLocale(Session::get('appLocale'));
        } else {
            App::setLocale(Config::get('app.fallback_locale'));
        }
        return $next($request);
    }
}