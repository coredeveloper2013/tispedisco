<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShipDetailsController extends Controller
{
    public function store(Request $request)
    {
        $shipDetails = $request->input()['shipmentInfo'];

        $shipDetails['totalHeight']         = 0;
        $shipDetails['totalWidth']          = 0;
        $shipDetails['totalLength']         = 0;
        $shipDetails['totalWeight']         = 0;
        $shipDetails['totalValue']          = 0;
        $shipDetails['totalAdditionalCost'] = 0;

        foreach ($shipDetails['packages'] as $package) {
            if ($package['height'] != null) {
                $shipDetails['totalHeight'] += $package['height'];
            }
            if ($package['width'] != null) {
                $shipDetails['totalWidth'] += $package['width'];
            }
            if ($package['length'] != null) {
                $shipDetails['totalLength'] += $package['length'];
            }
            if ($package['weight'] != null) {
                $shipDetails['totalWeight'] += $package['weight'];
            }

            if ($package['value'] != null) {
                $shipDetails['totalWeight'] += $package['value'];
            }
            if ($package['additional_cost'] != null) {
                $shipDetails['totalAdditionalCost'] += $package['additional_cost'];
            }
        }


        session(['shipDetails' => $shipDetails]);

        return response()->json(['status' => 200, 'msg'=>'Data has been stored successfully'], 200);
    }
}
