<?php

namespace App\Http\Controllers\StripeCon;

use App\Http\Controllers\Controller;
use App\User;

class StripeCustomerController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'verified']);

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

        if (env('APP_ENV') == 'production' )
            \Stripe\Stripe::setApiKey(env('STRIPE_PUBLISHABLE_KEY'));

        if (env('APP_ENV') == 'local' )
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
    }


    public function getCustomer($user = null)
    {
        if (!$user) {
            return ['success' => false, 'message' => 'Invalid user.'];
        }
        $responseData = $this->getCustomerId($user);
        if ($responseData['success'] == false) {
            return $responseData;
        }

        $customerId = $responseData['customerId'];

        try {
            $customer = \Stripe\Customer::retrieve($customerId);
            return ['success' => true, 'message' => 'Customer retrieved.', 'customer' => $customer];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    public function updateCustomer($updateData, $user = null)
    {
        if (!$user) {
            return ['success' => false, 'message' => 'Invalid user.'];
        }
        // validate input
        $validatedData = $updateData->validate([
            'name' => ['nullable', 'string', 'max:190'],
        ]);

        $responseData = $this->getCustomerId($user);

        if ($responseData['success'] == false) {
            return $responseData;
        }

        $customerId = $responseData['customerId'];

        try {
            $customer = \Stripe\Customer::update(
                $customerId,
                $validatedData
            );
            return ['success' => true, 'message' => 'Customer updated.', 'customer' => $customer];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    public function getCustomerId($user = null)
    {
        if (!$user) {
            return ['success' => false, 'message' => 'Invalid user.'];
        }
        if ($user && $user->stripe_customer_id) {
            return ['success' => true, 'message' => 'Customer already exist.', 'customerId' => $user->stripe_customer_id];
        }

        $responseData = $this->createCustomer($user);
        if ($responseData['success'] == false) {
            return $responseData;
        }
        return ['success' => true, 'message' => 'Customer id retrived.', 'customerId' => $responseData['customer']['id']];
    }

    public function createCustomer($user)
    {
        if (!$user) {
            return ['success' => false, 'message' => 'Invalid user.'];
        }

        $user = User::find($user->id);

        if (!$user) {
            return ['success' => false, 'message' => 'Invalid user.'];
        }

        $customer = [
            'name' => $user->first_name ?? null . $user->last_name ?? null,
            'email' => $user->email ?? null,
        ];

        try {
            $customer = \Stripe\Customer::create($customer);
            $user->update([
                'stripe_customer_id' => $customer->id
            ]);
            return ['success' => true, 'message' => 'Customer created succssfully', 'customer' => $customer];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }
}
