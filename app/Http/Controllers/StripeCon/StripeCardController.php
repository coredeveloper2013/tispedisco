<?php

namespace App\Http\Controllers\StripeCon;

use App\Http\Controllers\Controller;
use Validator;

class StripeCardController extends Controller
{
    public function __construct() {        
        $this->middleware(['auth', 'verified']);

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

        if (env('APP_ENV') == 'production' )
            \Stripe\Stripe::setApiKey(env('STRIPE_PUBLISHABLE_KEY'));

        if (env('APP_ENV') == 'local' )
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
    }

    /**
     * Get all card of a user
     *
     * @param App\User $user nullable
     * @return 'array with message and success{true, false}'
     */
    public function getCards($user = null)
    {
        if (!$user) {
            $user = auth()->user();
        }
        $responseData = app('\App\Http\Controllers\StripeCon\StripeCustomerController')->getCustomerId($user);  
        if ($responseData['success'] == false) {
            return $responseData;
        }

        $customerId = $responseData['customerId'];
        try {
            $cards = \Stripe\Customer::allSources(
                $customerId
            );
            return ['success' => true, 'message' => 'Customer card lists', 'cards' => $cards];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * Get all card of a user
     *
     * @param stripe\card\id $cardId
     * @param App\User $user nullable
     * @return 'array with message and success{true, false}'
     */
    public function getSingleCard($cardId = '', $user = null)
    {
        if (!$user) {
            $user = auth()->user();
        }
        $responseData = app('\App\Http\Controllers\StripeCon\StripeCustomerController')->getCustomerId($user);  
        if ($responseData['success'] == false) {
            return $responseData;
        }

        $customerId = $responseData['customerId'];
        try {
            $card = \Stripe\Customer::retrieveSource(
                $customerId,
                $cardId
            );
            return ['success' => true, 'message' => 'Customer card lists', 'card' => $card];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * Get all card of a user
     *
     * @param stripe\card\id $cardId
     * @param App\User $user nullable
     * @return 'array with message and success{true, false}'
     */
    public function deleteSingleCard($cardId = '', $user = null)
    {
        if (!$user) {
            $user = auth()->user();
        }
        $responseData = app('\App\Http\Controllers\StripeCon\StripeCustomerController')->getCustomerId($user);  
        if ($responseData['success'] == false) {
            return $responseData;
        }

        $customerId = $responseData['customerId'];
        try {
            $card = \Stripe\Customer::deleteSource(
                $customerId,
                $cardId
            );
            return ['success' => true, 'message' => 'Customer card lists', 'card' => $card];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * Create card
     *
     * @param stripeJs\token $token
     * @param App\User $user nullable
     * @return 'array with message and success{true, false}'
     */
    public function createCard($token, $user = null)
    {
        if (!$user) {
            $user = auth()->user();
        }
        $responseData = app('\App\Http\Controllers\StripeCon\StripeCustomerController')->getCustomerId($user);  
        if ($responseData['success'] == false) {
            return $responseData;
        }        
        $customerId = $responseData['customerId'];
        
        $token = [
            'source' => $token,
        ];
        
        try {
            $card = \Stripe\Customer::createSource(
                $customerId,
                $token
            );
            return ['success' => true, 'message' => 'Customer created succssfully', 'card' => $card];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }
    
    /**
     * Update card data
     *
     * @param stripe\card\id $cardId
     * @param stripe\card\updateData $updateData
     * @param App\User $user nullable
     * @return 'array with message and success{true, false}'
     */
    public function updateCard($cardId = '', $updateData = [], $user = null)
    {
        $validator = Validator::make($updateData, [
            "address_city" => 'nullable|string',
            "address_country" => 'nullable|string',
            "address_line1" => 'nullable|string',
            "address_line2" => 'nullable|string',
            "address_state" => 'nullable|string',
            "address_zip" => 'nullable|string',
            "exp_month" => 'nullable|numeric',
            "exp_year" => 'nullable|numeric', 
            "metadata" => 'nullable',
            "name" => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return ['success' => false, 'message' => $validator->errors()];
        }

        $validatedData = $validator->validate();

        if (!$user) {
            $user = auth()->user();
        }
        $responseData = app('\App\Http\Controllers\StripeCon\StripeCustomerController')->getCustomerId($user);  
        if ($responseData['success'] == false) {
            return $responseData;
        }        
        $customerId = $responseData['customerId'];

        try {
            $card = \Stripe\Customer::updateSource(
                $customerId,
                $cardId,
                $validatedData
            );
            return ['success' => true, 'message' => 'Customer created succssfully', 'card' => $card];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * Update card data
     *
     * @param cardData $cardData
     * @param App\User $user nullable
     * @return 'array with message and success{true, false}'
     */
    public function attachCard($user, $cardData)
    {
        try {        
            $backendToken = \Stripe\Token::create([
                'card' => [
                    'number' => $cardData['credit_card_no'],
                    'exp_month' => $cardData['card_exp_month'],
                    'exp_year' => $cardData['card_exp_year'],
                    'cvc' => $cardData['card_cvc'],
                ],
            ]);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }

        $responseData = $this->getCards($user);
        $cards = $responseData['cards']['data'];
        $actualCard = $backendToken;
        $found = false;

        foreach ($cards as $card) {
            if ($actualCard['card']['brand'] == $card['brand']
                && $actualCard['card']['last4'] == $card['last4']
                && $actualCard['card']['fingerprint'] == $card['fingerprint']) {
                    $actualCard = $card;
                    $found = true;
                    break;
            }
        }

        if ($found) {            
            return ['success' => true, 'message' => 'Card from list.', 'card' => $card];
        }
        $token = $actualCard['id'];
        return $this->createCard($token, $user);
    }
}
