<?php

namespace App\Http\Controllers\BackEndCon;

use App\DataTables\TicketsDataTable;
use App\Http\Controllers\Controller;
use App\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TicketsDataTable $ticket, Request $request)
    {
        return $ticket->setData($request->all())->render('Admin.tickets.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::find($id);
        return view('Admin.tickets.single', compact('ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = Ticket::find($id)->delete();
        if ($ticket) {
            return response()->json(['success' => true, 'message' => "Successful"], 200);
        } else {
            return response()->json(['success' => false, 'message' => "Something Went wrong!"], 200);
        }
    }

    public function changeStatus(Request $request)
    {
        $ticket = Ticket::findOrFail($request->id);
        if ($request->status == 0) {
            $status = 1;
        } else {
            $status = 0;
        }
        $update = $ticket->update(['status' => $status]);
        if ($update) {
            return response()->json(['success' => true, 'message' => "Order Status Changed"], 200);
        } else {
            return response()->json(['success' => false, 'message' => "Something Went wrong!"], 200);
        }
    }
}
