<?php

namespace App\Http\Controllers\BackEndCon;

use App\Carrier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarrierSettingsController extends Controller
{

    public function index()
    {
        $carriers = Carrier::get();

        return view('Admin.settings.carrier.index', compact('carriers'));
    }


    public function create()
    {
        return view('Admin.settings.carrier.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'fee' => 'required',
            'configs' => 'required',
            'logo' => 'image|mimes:jpeg,bmp,png',
        ]);

        $carrier =  new Carrier;

        if ($request->logo) {
            $carrier->logo = $request->logo->store('uploads/carriers', ['disk' => 'public_folder']);
        }

        $carrier->title = $request->title;
        $carrier->fee = $request->fee;

        $configs = [];
        foreach ($request->configs as $config) {
            $configs[$config['name']] = $config['value'];
        }

        $carrier->configs = json_encode($configs);
        $carrier->save();

        return redirect()->route('admin.carriers.index')->with('success', 'API settings created successfully.');
    }


    public function edit($id)
    {
        $carrier = Carrier::find($id);

        return view('Admin.settings.carrier.edit', compact('carrier'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fee' => 'required',
            'title' => 'required',
            'configs' => 'required',
            'logo' => 'image|mimes:jpeg,bmp,png',
        ]);

        $carrier =  Carrier::find($id);

        if (file_exists(asset($carrier->logo))) {
            unlink(asset($carrier->logo));
        }

        if ($request->logo) {
            $carrier->logo = $request->logo->store('uploads/carriers', ['disk' => 'public_folder']);
        }

        $carrier->title = $request->title;
        $carrier->fee = $request->fee;
        $configs = [];

        foreach ($request->configs as $config) {
            $configs[$config['name']] = $config['value'];
        }

        $carrier->configs = json_encode($configs);
        $carrier->save();

        return redirect()->back()->with('success', 'API settings updated successfully.');
    }


    public function destroy($id)
    {
        try {
            $carrier = Carrier::find($id);

            if (file_exists(asset($carrier->logo))) {
                unlink(asset($carrier->logo));
            }

            $carrier->delete();

            return redirect()->back()->with('success', 'API settings deleted successfully.');

        } catch (Exception $e) {

            return redirect()->back()->with('error', $e->getMessage());

        }

    }

}
