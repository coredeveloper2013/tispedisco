<?php

namespace App\Http\Controllers\FrontEndCon;

use App\Http\Controllers\Controller;
use App\Carrier;
use App\Order;
use App\orderItem;
use App\Rate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class PaymentDesignController extends Controller
{
    public function index()
    {
        return view('User.payment-design');
    }

    public function store(Request $request)
    {
        $this->storeSave($request);
    }

    public function storeSave($paymentDetails)
    {
        $user = auth()->guard('web')->user();
        if($user) {
            return 1;
            $responseData = app('\App\Http\Controllers\StripeCon\StripeCustomerController')->getCustomerId($user);

            return $responseData;

            if ($responseData['success'] == false) {
                return $responseData;
            }
        }

        try {
            DB::beginTransaction();

            unset($paymentDetails['_token']);
            session(['paymentDetails' => $paymentDetails]);

            $shipDetails    = session('shipDetails');
            $serviceDetails = session('selected_service');
            $sender         = session('shippingAddress')['sender'];
            $recipient      = session('shippingAddress')['recipient'];

            $paymentDetails = session('paymentDetails');
            $total_costs    = session('total_costs');



            $order_data = array(
                'collection_date'     => $shipDetails['collection_date'],
                'length'              => $shipDetails['totalLength'],
                'width'               => $shipDetails['totalWidth'],
                'height'              => $shipDetails['totalHeight'],
                'weight'              => $shipDetails['totalWeight'],
                'volume'              => $shipDetails['totalLength'] * $shipDetails['totalWidth'] * $shipDetails['totalHeight'],

                'sender_full_name'    => $sender['first_name'] . ' ' . $sender['last_name'],
                'sender_email'        => $sender['email'],
                'sender_company_name' => $sender['company_name'],
                'sender_pec_address'  => $sender['pec_address'],
                'sender_sdi_code'     => $sender['sdi_code'],
                'sender_vat_no'       => $sender['vat_no'],
                'sender_phone'        => $sender['phone'],
                'sender_address_1'    => $sender['address_1'],
                'sender_address_2'    => $sender['address_2'],
                'sender_city'         => $sender['city'],
                'sender_province'     => $sender['province'],
                'sender_postcode'     => $sender['post_code'],
                'sender_country'      => $sender['country'],

                'billing_phone'       => $sender['phone'],
                'billing_address_1'   => $sender['address_1'],
                'billing_address_2'   => $sender['address_2'],
                'billing_city'        => $sender['city'],
                'billing_province'    => $sender['province'],
                'billing_postcode'    => $sender['post_code'],
                'billing_country'     => $sender['country'],

                'departure_phone'     => $sender['phone'],
                'departure_address_1' => $sender['address_1'],
                'departure_address_2' => $sender['address_2'],
                'departure_city'      => $sender['city'],
                'departure_province'  => $sender['province'],
                'departure_postcode'  => $sender['post_code'],
                'departure_country'   => $sender['country'],

                'receiver_full_name'  => $recipient['first_name'] . ' ' . $recipient['last_name'],
                'receiver_phone'      => null,
                'receiver_email'      => $recipient['email'],
                'receiver_address_1'  => $recipient['address_1'],
                'receiver_address_2'  => $recipient['address_2'],
                'receiver_city'       => $recipient['city'],
                'receiver_province'   => $recipient['province'],
                'receiver_postcode'   => $recipient['post_code'],
                'receiver_country'    => $recipient['country'],

                'additional_cost'     => $shipDetails['totalAdditionalCost'],

                'price'               => $serviceDetails['TotalCost'],
                'total_cost'          => $serviceDetails['TotalCost'],

                'description'         => $shipDetails['total_comment'],
                'possible_notes'      => $paymentDetails['possible_notes'],
            );

            $order = Order::create($order_data);

            if ($order) {
                $updated_order = Order::find($order->id)->update(['order_code' => Carbon::now()->format('YmdHis') . $order->id]);
                if ($updated_order) {
                    session(['order' => $order]);
                    $order_items_data = [];
                    if ($shipDetails['packages']) {
                        foreach ($shipDetails['packages'] as $package) {
                            $volume = $package['length'] * $package['height'] * $package['width'];
                            $data = array(
                                'shipment_name'   => $package['name'],
                                'length'          => $package['length'] ?? 0,
                                'height'          => $package['height'] ?? 0,
                                'width'           => $package['width'] ?? 0,
                                'volume'          => $volume,
                                'weight'          => $package['weight'] ?? 0,
                                'price'           => $package['value'],
                                'vat'             => $this->getVatPercentage($serviceDetails['name']),
                                'additional_cost' => $package['additional_cost'],
                                'total_cost'      => 0,
                                'description'     => $package['comment'],
                            );
                            $order_items_data[] = new OrderItem($data);
                        }
                    }
                    $order_items = $order->items()->saveMany($order_items_data, true);
                }
                $dataVariable = [
                    'amount'   => (int) ($order->total_cost),
                    'currency' => 'usd'
                ];
                $token        = $paymentDetails['token'];
                $responseData = app('\App\Http\Controllers\StripeCon\StripePaymentController')->createPayment($token, $dataVariable, $user);

                if ($responseData['success'] == false) {
                    DB::rollBack();
                    return $responseData;
                }
                DB::commit();

                return ['success' => true, 'message' => 'All action successful.'];
            }

        } catch (\Exception $e){
            DB::rollBack();
            return $e;
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }

    }

    private function getVatPercentage($carrierKey){
        $carrier = Carrier::where('title', 'LIKE', '%'. $carrierKey .'%')->first();

        if (!$carrier->fee) {
            return 15;
        };

        return $carrier->fee;
    }
}
