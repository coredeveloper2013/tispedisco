<?php

namespace App\Http\Controllers\FrontEndCon;

use App\Carrier;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\FedexService;

class ShipDetailsController extends Controller
{

    public function index()
    {
        if (!session('locations') || !session('selected_service')) {
            return redirect()->route('root')->with('error', 'No location or selected carrer data found.');
        }

        return view('User.ship-details');
    }

    public function updateCost(Request $request)
    {
        $this->validate($request, [
            'packages' => $request->packages
        ]);

        $service = session('selected_service');
        $address = session('locations');

        $orderData = [
            'fromAddressLine' => $address['location_from']['formatted_address'],
            'fromPostalCode'  => $address['fromPostalCode'],
            'fromCity'        => $address['fromCity'],
            'fromCountryCode' => $address['fromCountryCode'],

            'toAddressLine'   => $address['location_to']['formatted_address'],
            'toPostalCode'    => $address['toPostalCode'],
            'toCity'          => $address['toCity'],
            'toCountryCode'   => $address['toCountryCode'],

            'ServiceType' => $service['ServiceType'],
            'PackagingType' => $service['PackagingType'],
            'RateType' => $service['RateType'],

            'packages' => $request->packages
        ];


        $carrierFee = Carrier::where('title', session('selected_service')['name'])->first('fee')->fee;
        $costData = FedexService::getAccurateCost($orderData);

        if($costData['status'] == true){
            $selected_service = session('selected_service');

            $selected_service['CarrierCost'] = $costData['cost'];
            $selected_service['CarrierSurcharges'] = $costData['CarrierSurcharges'];

            $cost = $costData['cost'] + (($costData['cost'] * $carrierFee) / 100);

            $selected_service['TotalCost'] = $cost;
            session(['selected_service' => $selected_service]);

            return [
                'status' => true,
                'cost' => number_format($cost, 2)
            ];
        }

        return $costData;
    }

    public function store(Request $request)
    {

        $selected_service = $request->all();
        unset($selected_service['_token']);

        session($selected_service);

        return redirect()->route('ship-details.index');

    }

}
