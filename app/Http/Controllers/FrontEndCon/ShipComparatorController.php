<?php

namespace App\Http\Controllers\FrontEndCon;

use App\Carrier;
use App\Http\Controllers\Controller;
use App\Rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FrontEndCon\CarrierController;
use function foo\func;

class ShipComparatorController extends Controller
{
    public function index()
    {
        $locations = session('locations');

        if (!$locations) {
            return redirect()->route('root')->with('error', 'No location data found.');
        }

        $orderData = [
            'fromAddressLine' => $locations['location_from']['formatted_address'],
            'fromPostalCode'  => $locations['fromPostalCode'],
            'fromCity'        => $locations['fromCity'],
            'fromCountryCode' => $locations['fromCountryCode'],

            'toAddressLine'   => $locations['location_to']['formatted_address'],
            'toPostalCode'    => $locations['toPostalCode'],
            'toCity'          => $locations['toCity'],
            'toCountryCode'   => $locations['toCountryCode'],
            'packages'        => [
                [
                    'weight' => $locations['weight']
                ]
            ]
        ];


        // Call FedEx rate api and get the reuslts
        $fedexServices = CarrierController::getServices($orderData);

        $carrier = Carrier::where('title', 'Fedex')->first();

        $services = [];

        if (count($fedexServices['data']) > 0) {
            $services = $fedexServices['data'];
        }

        return view('User.ship-comparator', compact('services', 'locations', 'carrier'));
    }


    public function selectedCarrier(Request $request)
    {
        session(['selected_carrier' => $request->selected_carrier]);
        return redirect(route('ship-details.index'));
    }


    public function store(Request $request)
    {
        $locations = array(
            'from'     => $request->from,
            'to'       => $request->to,
            'size'     => $request->size,
            'distance' => abs($request->from - $request->to),
        );

        session(['locations' => $locations]);
        $locations = session('locations');

        $carriers = Carrier::with(['rates' => function ($rates) use ($locations) {
            $rates->where('distance_from', '<=', $locations['distance']);
            $rates->where('distance_to', '>=', $locations['distance']);
            $rates->where('volume', '>=', $locations['size']);
        }])->get();

        return view('User.shipComparator', compact('locations', 'carriers'));
    }

}
