<?php

namespace App\Http\Controllers\FrontEndCon;

use App\Carrier;
use Carbon\Carbon;
use App\Services\FedexService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarrierController extends Controller
{
    /**
	 * Order Variable Structure: Array
	 *    - Shipper: array - contains shipper data
	 * 			- PostalCode: required
	 *    - Recipient: array - contains recipient data
	 *    - Packages: array of packages
	 * 	  	-	Weight: array
	 * 	  	-	Height: array
	 */

    public static function getServices($orderData = null)
    {
    	return FedexService::getServices($orderData);
    }

    public function getQuotes()
    {
    	// 510 W Reno Ave, Oklahoma City, OK 73102, United States
    	$orderData = [
            'Weight'          => 10,

            'fromAddressLine' => 'Shamoli',
            'fromPostalCode'  => 1207,
            'fromCity'        => 'Dhaka',
            'fromCountryCode' => 'BD',

            'toAddressLine'   => '510 W Reno Ave',
            'toPostalCode'    => 73102,
            'toCity'          => 'Oklahoma',
            'toCountryCode'   => 'US',

            'packages' => [[]]
        ];

    	return response()->json(FedexService::getServices($orderData));
    }

    public function validateAddress()
    {
    	$address = [
			'StreetLines' => '510 W Reno Ave',
			'City' => 'Oklahoma',
			'StateOrProvinceCode' => '',
			'PostalCode' => 73102,
			'CountryCode' => 'US',
		];

    	return FedexService::validateAddress($address);
    }


    public function createShipping()
    {
    	$shippingData = [
			'CommodityName'     => 'DOCUMENTS',
			'PackagingType'     => 'YOUR_PACKAGING',
			'ServiceType'       => 'INTERNATIONAL_PRIORITY',
			'expectedTimestamp' => '2019-12-27T10:30:00',
    		'sender' => [
				'CompanyName'  => 'CompanyName',
				'EMailAddress' => 'hasan@dev.com',
				'PersonName'   => 'Hasan Mahmud',
				'StateOrProvinceCode' => '',
				'PhoneNumber'  => '+880135142370',
				'StreetLines'  => 'Shamoli',
				'PostalCode'   => 1207,
				'City'         => 'Dhaka',
				'CountryCode'  => 'BD',
    		],
    		'recipient' => [
				'PersonName'          => 'Greta Becker',
				'PhoneNumber'         => '+467868435',
				'StreetLines'         => '510 W Reno Ave',
				'City'                => 'Oklahoma',
				'StateOrProvinceCode' => 'OK',
				'PostalCode'          => 73102,
				'CountryCode'         => 'US',
    		],
    	];

    	return FedexService::createShipingRequest($shippingData);
    }


}
