<?php

namespace App\Http\Controllers\FrontEndCon;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::with('profile', 'billingAddress')->findOrfail(auth()->id());
        return view('User.profile.pages.profile', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->input());
        $user = User::findOrFail($id);
        $userData = $request->validate([
            "first_name" => '',
            "last_name" => '',
            "profile_phone" => '',
            "profile_address_1" => '',
            "profile_address_2" => '',
            "profile_city" => '',
            "profile_province" => '',
            "profile_postcode" => '',
            "profile_country" => '',
            "company_name" => '',
            "pec_address" => '',
            "sdi_code" => '',
            "vat_no" => '',
            "phone" => '',
            "address_1" => '',
            "address_2" => '',
            "city" => '',
            "province" => '',
            "postcode" => '',
            "country" => '',
        ]);
        $userSelfData = array(
            "first_name" => $userData['first_name'],
            "last_name" => $userData['last_name'],
        );
        $profileData = array(
            "phone" => $userData['profile_phone'],
            "address_1" => $userData['profile_address_1'],
            "address_2" => $userData['profile_address_2'],
            "city" => $userData['profile_city'],
            "province" => $userData['profile_province'],
            "postcode" => $userData['profile_postcode'],
            "country" => $userData['profile_country'],
        );
        $billingAddressData = array(
            "company_name" => $userData['company_name'],
            "pec_address" => $userData['pec_address'],
            "sdi_code" => $userData['sdi_code'],
            "vat_no" => $userData['vat_no'],
            "phone" => $userData['phone'],
            "address_1" => $userData['address_1'],
            "address_2" => $userData['address_2'],
            "city" => $userData['city'],
            "province" => $userData['province'],
            "postcode" => $userData['postcode'],
            "country" => $userData['country'],
        );
        $update = $user->update($userSelfData);
        session()->flash('Whoops! Something went wrong');
        if ($update){
            $update = $user->profile()->updateOrCreate(['user_id' => $id], $profileData);
            if ($update) {
                $update = $user->billingAddress()->updateOrCreate(['user_id' => $id], $billingAddressData);
                if ($update){
                    session()->flash('Profile Updated Successfully');
                }
            }
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
