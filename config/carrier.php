<?php 
	
	/**
	 * This configuration will contain all the credentials of the carriers
	 */

	return [
			'fedex' => [
				'fedex_in_production' => env('FEDEX_IN_PRODUCTION', false),
				'fedex_key' => env('FEDEX_KEY', ''),
				'fedex_password' => env('FEDEX_PASSWORD', ''),
				'fedex_account_number' => env('FEDEX_ACCOUNT_NUMBER', ''),
				'fedex_meter_number' => env('FEDEX_METER_NUMBER', ''),
			]

	];