<?php

// Language Routes
Route::get('lang/{langKey}', function ($langKey) {
    if (array_key_exists($langKey, Config::get('languages'))) {
        Session::put('appLocale', $langKey);
    }
    return redirect()->back();
})->name('changeLanguage');


Route::get('/sess', function () {
    dd(session()->all());
    return session()->all();
});


Route::group(['namespace' => 'StripeCon', 'middleware' => 'verified', 'prefix' => 'stripe'], function () {

    Route::get('get-cards', 'UserStripeController@getCards')
        ->name('user.getCards');

    Route::post('update-card', 'UserStripeController@updateCard')
        ->name('user.updateCard');

    Route::post('get-single-card', 'UserStripeController@getSingleCard')
        ->name('user.getSingleCard');

    Route::get('get-single-customer', 'UserStripeController@getCustomer')
        ->name('user.getSingleCustomer');
});

Route::middleware('guest:admin', function () {

});


Auth::routes(['verify' => true]);


Route::middleware('guest', function () {

    Route::get('/login', 'Auth\LoginController@showLoginForm')
        ->name('login');

    Route::get('/register', 'Auth\RegisterController@register')
        ->name('register');


    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')
        ->name('password.request');

    Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')
        ->name('password.reset');

});




Route::get('/email/verify', 'Auth\VerificationController@show')->name('verification.notice');

Route::group(['namespace' => 'FrontEndCon'], function () {

    // Fedex endpoints for testing purpose
    Route::get('/fedex/get-quotes', 'CarrierController@getQuotes')
        ->name('fedex.getQuotes');

    Route::get('/fedex/create-shipping', 'CarrierController@createShipping')
        ->name('fedex.createShipping');

    Route::get('/fedex/validate-address', 'CarrierController@validateAddress')
        ->name('fedex.validateAddress');




    Route::get('/', 'HomeController@index')
        ->name('root');

    Route::get('/home', 'UserController@home')
        ->name('home');

    Route::resource('ship-comparator', 'ShipComparatorController');

    Route::post('selected-carrier', 'ShipComparatorController@selectedCarrier')
        ->name('selected-carrier');

    Route::resource('ship-details', 'ShipDetailsController');

    Route::post('ship-details/updateCost', 'ShipDetailsController@updateCost')
        ->name('ship-details.updateCost');


    Route::resource('ship-address', 'ShipAddressController');

    Route::resource('payment-design', 'PaymentDesignController');

    Route::resource('invoice', 'InvoiceController');

    Route::resource('order-confirm', 'OrderConfirmController');


    Route::group(['middleware' => 'verified', 'prefix' => 'user'], function () {

        Route::get('/', 'HomeController@index');

        Route::post('/single-update', 'UserController@singleUpdate')
            ->name('profile.singleUpdate');



        // User routes
        Route::group(['as' => 'user.'], function() {
            Route::resource('order', 'UserOrderController');

            Route::resource('address', 'UserAddressController');
            Route::resource('credit-card', 'UserCardController');
            Route::resource('profile', 'UserProfileController');
            Route::resource('fatture', 'UserInvoiceController');

            Route::get('/dashboard', 'UserController@dashboard')
                ->name('dashboard');

            Route::get('/pass-change', 'UserController@passChange')
                ->name('passChange');

            Route::get('/ticket', 'TicketController@ticket')
                ->name('ticket');

            Route::get('/tickets', 'TicketController@getTickets')
                ->name('getTickets');

            Route::get('/single-ticket', 'TicketController@singleTicket')
                ->name('singleTicket');
        });


        Route::get('/email/change/{token}', 'UserController@emailChange')
            ->name('changeEmail.confirm');

        Route::post('/ticket', 'TicketController@cruTicket')
            ->name('cru.ticket');

        Route::post('/reply', 'TicketController@cruReply')
            ->name('cru.reply');

        Route::post('/file-upload', 'TicketController@fileUpload')
            ->name('ticket.fileUpload');
    });
});

Route::group(['prefix' => 'api/v0.1', 'as' => 'api.'], function () {
    Route::resource('ship-details', 'Api\ShipDetailsController');
});


// Custom page route goes here
Route::get('/{slug}', 'HomeController@slug')
    ->name('page.show');
