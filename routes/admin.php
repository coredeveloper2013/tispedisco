<?php


Route::group(['as' => 'admin.', 'middleware' => 'guest:admin'], function () {

    Route::get('login', 'Auth\AdminLoginController@showLoginForm')
        ->name('login');

    Route::post('login', 'Auth\AdminLoginController@login')
        ->name('login');

});



Route::group(['as' => 'admin.', 'middleware' => 'auth:admin'], function () {

    Route::post('logout', 'Auth\AdminLoginController@logout')
        ->name('logout');

    Route::get('profile/change-password', 'ProfileController@changePasswordView')
        ->name('profile.changePassword');

    Route::post('profile/change-password', 'ProfileController@changePassword')
        ->name('profile.changePassword');

    Route::get('/', 'DashboardController@index')
        ->name('dashboard');

    Route::resource('users', 'UserController');
    Route::post('users/change-status', 'UserController@changeStatus')
        ->name('users.change-status');

    Route::resource('order', 'OrderController');
    Route::post('order/change-status', 'OrderController@changeStatus')
        ->name('order.change-status');

    Route::resource('tickets', 'TicketController');
    Route::resource('pages', 'PageController');


    Route::prefix('settings')->group(function () {

        Route::resource('carriers', 'CarrierSettingsController');

        Route::post('create-language', 'LanguageSettingsController@createLanguage')
            ->name('create-language');
        
    });



});

Route::get('/h', function () {
    return view('Admin.invoice.index');
});
