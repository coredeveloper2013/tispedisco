-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table tispedisco.addresses
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_user_id_foreign` (`user_id`),
  CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.addresses: ~50 rows (approximately)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` (`id`, `first_name`, `last_name`, `email`, `address_1`, `address_2`, `city`, `country`, `state`, `province`, `postcode`, `phone`, `user_id`, `created_at`, `updated_at`) VALUES
    (1, 'Madie', 'Romaguera', 'bhegmann@maggio.com', 'Suite 434', '93061 Felicia Plains Suite 078\nLake Christyborough, NJ 05485-5287', 'West Hallie', 'Palestinian Territories', 'South Carolina', 'sr_RS', '35595-3972', '(548) 608-5743', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (2, 'Cortez', 'Corwin', 'vkoch@gmail.com', 'Suite 667', '46026 Crooks Park\nNew Paulport, ME 84745', 'Isabelleside', 'Nicaragua', 'Mississippi', 'ar_YE', '83042-4500', '868.492.4218 x259', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (3, 'Consuelo', 'Kiehn', 'jacky07@hotmail.com', 'Apt. 688', '777 Huel Camp\nGoyettechester, RI 91045-7398', 'Izaiahchester', 'Mauritania', 'California', 'kfo_CI', '69794', '1-243-815-1573 x1910', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (4, 'Marisa', 'Collier', 'enos42@lynch.info', 'Suite 736', '2486 Rohan Row\nSashafurt, KS 36344-4777', 'Lake Odessashire', 'Algeria', 'New Mexico', 'km_KH', '05281', '470-938-1427 x3169', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (5, 'Selena', 'Walter', 'dubuque.dayne@yahoo.com', 'Apt. 243', '5128 Hane Turnpike\nGreenfurt, CO 72374-0256', 'Port Cortezbury', 'France', 'Arizona', 'es_CR', '61353', '1-363-347-6361 x2088', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (6, 'Casandra', 'Corkery', 'fadel.ryan@yahoo.com', 'Apt. 958', '831 Bergnaum Plaza\nTatyanafort, KS 05841', 'Brekkestad', 'Christmas Island', 'Ohio', 'ee_TG', '02202-1214', '1-489-487-1797 x500', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (7, 'Ludwig', 'Shanahan', 'agustina44@jacobson.info', 'Apt. 620', '804 Stanton Walk\nEast Lunamouth, NV 69138', 'New Madgestad', 'Ethiopia', 'Georgia', 'ar_DZ', '56788', '(292) 676-1521 x129', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (8, 'Osbaldo', 'Stroman', 'jennie02@gmail.com', 'Apt. 536', '8102 Sadye Hill Apt. 687\nRosenbaumchester, AZ 23656-1144', 'Bruenmouth', 'Turks and Caicos Islands', 'Utah', 'es_PA', '56195', '658-443-3864 x560', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (9, 'April', 'Lemke', 'rowe.lonnie@lesch.com', 'Suite 441', '22573 Isaiah Cliffs\nColestad, IA 65654-4792', 'Audieburgh', 'San Marino', 'Louisiana', 'oc_FR', '30292-2768', '424-516-4885 x67871', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (10, 'Jessy', 'Marquardt', 'jettie07@gmail.com', 'Apt. 112', '352 Kyler Terrace\nBernhardmouth, FL 81831', 'East Beaumouth', 'Netherlands', 'Georgia', 'ko_KR', '39032', '+1-410-461-9218', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (11, 'Gonzalo', 'Emmerich', 'quinn52@gibson.com', 'Apt. 787', '26501 Bosco Loaf Suite 656\nElzaberg, TX 12129-3383', 'Lindgrenmouth', 'Samoa', 'Arizona', 'da_DK', '49346', '(770) 922-8194 x589', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (12, 'Donny', 'Baumbach', 'leuschke.mariah@hotmail.com', 'Apt. 078', '1343 Mabel Shoal Apt. 619\nPort Lunahaven, NC 47196-4851', 'Kassulkeside', 'Taiwan', 'Indiana', 'sq_AL', '22100', '+1.678.336.5080', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (13, 'Nia', 'Windler', 'ggreenfelder@gmail.com', 'Apt. 700', '307 Bradtke Trace Suite 852\nWeberhaven, MN 64820-5321', 'West Murphyside', 'Estonia', 'Nebraska', 'es_DO', '05850-0292', '+1 (547) 591-3428', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (14, 'Bettye', 'Medhurst', 'kuhlman.coralie@yahoo.com', 'Apt. 590', '11194 Winona Pines\nHeidenreichshire, WA 63227-1065', 'Alekhaven', 'Brunei Darussalam', 'Arizona', 'sh_BA', '92261', '+18175883635', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (15, 'Brenden', 'Dare', 'lew.schulist@konopelski.com', 'Apt. 472', '753 Littel Gateway\nPort Mathew, CO 30611-8235', 'Nicholausport', 'Georgia', 'Mississippi', 'ku_SY', '04395-8323', '1-783-585-3886', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (16, 'Mckenzie', 'Wintheiser', 'nelson79@stiedemann.biz', 'Apt. 218', '88492 Aufderhar Cliff\nKossmouth, AK 14500', 'Rogahnberg', 'Tanzania', 'Colorado', 'sa_IN', '52486', '1-924-924-2130 x68234', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (17, 'Merle', 'Anderson', 'stiedemann.josefina@ondricka.com', 'Apt. 694', '594 O\'Conner Squares\nTeresaside, OH 58940', 'New Lucio', 'Korea', 'Tennessee', 'ti_ET', '17855', '930.590.3973', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (18, 'Bret', 'Pouros', 'dee47@hotmail.com', 'Apt. 849', '4703 Fay Summit Apt. 082\nWest Dixiefort, ND 94260-7799', 'North Ludie', 'Holy See (Vatican City State)', 'Illinois', 'so_ET', '10714', '+1-949-305-8261', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (19, 'Bruce', 'Daugherty', 'rowland.fahey@gmail.com', 'Suite 437', '53396 Mann Lodge Suite 096\nErdmanfort, HI 38433-3876', 'Vivianneton', 'Sierra Leone', 'North Carolina', 'ee_GH', '38427', '1-687-318-1518', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (20, 'Kaleigh', 'Carter', 'danyka.olson@gmail.com', 'Suite 642', '218 Emmerich Glens\nTryciashire, KY 81485-5407', 'South Arely', 'Belarus', 'Vermont', 'el_GR', '83954-7768', '+1.243.517.6945', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (21, 'Kaci', 'Krajcik', 'tristian.monahan@yahoo.com', 'Apt. 329', '1226 Audrey Camp\nSouth Susanna, VT 16973', 'Lupeville', 'Norfolk Island', 'South Dakota', 'es_PA', '28806', '232.800.3069 x2068', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (22, 'Adeline', 'Welch', 'jayme.carroll@willms.biz', 'Suite 074', '655 Morton Dam\nNedramouth, MN 97831-7902', 'Camillashire', 'Estonia', 'Indiana', 'hi_IN', '43609', '(276) 581-1601', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (23, 'Frederique', 'Brekke', 'imani.morissette@runte.org', 'Suite 822', '77890 Zulauf Gardens Suite 115\nErichfort, MN 43470-5972', 'South Greenburgh', 'Syrian Arab Republic', 'Arizona', 'en_CA', '37830', '(436) 477-6332 x7315', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (24, 'Jada', 'Eichmann', 'flavie72@barton.info', 'Suite 013', '1249 Katarina Lodge Apt. 997\nFredborough, KS 46217-0173', 'Wiegandport', 'Canada', 'Pennsylvania', 'ar_DZ', '27560', '(814) 746-3924 x776', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (25, 'Loma', 'Feest', 'elenor.willms@gmail.com', 'Apt. 887', '161 Nikolaus Junctions\nLake Allen, KY 38779', 'Andersonville', 'Iran', 'Alaska', 'ts_ZA', '12500', '1-590-365-6877', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (26, 'Hubert', 'Cremin', 'lillian.huels@howe.com', 'Suite 205', '867 Roberta Islands Apt. 289\nCierraside, PA 28708', 'South Jazmyne', 'Japan', 'Nebraska', 'rw_RW', '01473', '(438) 394-5166 x879', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (27, 'Laila', 'Boyle', 'nayeli.goldner@yahoo.com', 'Suite 512', '3861 Nigel Forge Apt. 397\nNorth Arturomouth, MD 29425', 'South Averytown', 'Timor-Leste', 'Rhode Island', 'so_KE', '50189', '(283) 615-6342 x677', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (28, 'Ramon', 'Cormier', 'dorian.bartell@hotmail.com', 'Suite 851', '12281 Herta Corners\nKrystalberg, NV 01943-2427', 'New Erling', 'Bahrain', 'District of Columbia', 'ar_QA', '69957-3118', '302.832.0177 x67708', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (29, 'Kiley', 'Stark', 'cassandra43@lind.com', 'Suite 710', '8433 Jacobi Square\nLake Aftonton, MO 13789-5307', 'North Lennie', 'Costa Rica', 'Minnesota', 'trv_TW', '14984', '1-541-820-5241 x97266', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (30, 'Mariam', 'Harris', 'nitzsche.edgardo@gutmann.biz', 'Suite 955', '72965 Robel Views Apt. 233\nNorth Haydenmouth, ID 75663-3933', 'Lake Kathryneville', 'Jersey', 'Ohio', 'nl_BE', '74133-9335', '826.222.7071 x9666', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (31, 'Oswald', 'Cormier', 'kelley99@marquardt.info', 'Apt. 086', '70639 Jakubowski Parks\nD\'Amoreberg, NJ 04684-5673', 'Devonteshire', 'Mexico', 'Washington', 'kl_GL', '51308', '+1-558-958-2354', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (32, 'Brooks', 'Lind', 'mhermann@hotmail.com', 'Apt. 462', '48618 Graham Flats Apt. 198\nPort Deborahville, MI 05535-5975', 'Port Lila', 'Jersey', 'North Carolina', 'es_CO', '72399-8041', '942-684-2928', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (33, 'Frankie', 'Ratke', 'kub.chris@yahoo.com', 'Suite 533', '266 Ressie Pike Apt. 007\nSouth Hortensetown, NJ 22809-7948', 'South Marjoriehaven', 'Latvia', 'Oregon', 'uz_UZ', '37790', '1-548-360-6697 x468', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (34, 'Alex', 'Bayer', 'dillan.okuneva@hotmail.com', 'Suite 375', '93605 Buckridge Junctions\nNorth Mose, MI 41267', 'Bertafort', 'Kyrgyz Republic', 'Colorado', 'fr_MC', '86836-2140', '1-782-393-3949', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (35, 'Reuben', 'Gaylord', 'eloisa75@hotmail.com', 'Suite 416', '605 Lisette Loop Apt. 650\nJaylonview, MI 36089', 'West Alexandreberg', 'Samoa', 'Oregon', 'pa_IN', '06366-7101', '(764) 595-7968 x510', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (36, 'Buddy', 'Kessler', 'santos.larson@bauch.biz', 'Apt. 710', '5897 Feeney Stravenue Apt. 534\nRobertsview, VT 67503', 'Pagacport', 'Pitcairn Islands', 'Arkansas', 'pt_BR', '74828-0382', '1-761-436-3630', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (37, 'Judd', 'Haag', 'wendell.bashirian@yahoo.com', 'Apt. 658', '202 Hodkiewicz Squares Apt. 089\nBeckerton, WA 28272', 'North Cassidy', 'Portugal', 'Pennsylvania', 'dv_MV', '49622', '487.991.7534', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (38, 'Josiah', 'Hudson', 'dejon33@hotmail.com', 'Suite 932', '95254 Koch Mission Suite 848\nElwyntown, DC 88434', 'Rowlandshire', 'Korea', 'District of Columbia', 'sid_ET', '99952', '432.717.3156 x233', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (39, 'Marshall', 'Hackett', 'elmo.runte@marquardt.com', 'Apt. 245', '7085 Rice Neck\nParisianmouth, UT 02772', 'Port Nehaville', 'Qatar', 'Arizona', 'pa_IN', '51573-7976', '342.878.1482', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (40, 'Tommie', 'Dietrich', 'goyette.malachi@hotmail.com', 'Suite 794', '20862 O\'Kon Junction Apt. 991\nKozeybury, NE 49677', 'Jaylanstad', 'Colombia', 'Virginia', 'bn_BD', '02823', '415-883-8826 x84662', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (41, 'Sigrid', 'Volkman', 'nolan.quinten@pouros.com', 'Suite 646', '34777 Ross Roads\nNew Presley, FL 63808-9215', 'Port Hunter', 'Central African Republic', 'Illinois', 'es_HN', '22415', '(335) 219-9209 x998', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (42, 'Charity', 'Harris', 'alebsack@leffler.biz', 'Apt. 554', '6560 Lindgren Plain\nJarvischester, ND 33570', 'East Kamryn', 'Serbia', 'Indiana', 'fr_CA', '02451-8839', '+1-335-379-4947', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (43, 'Lauretta', 'Johnston', 'rhoda42@dach.com', 'Apt. 115', '1162 Liana Curve\nDevanfort, OR 19157', 'East Citlallichester', 'Uganda', 'Mississippi', 'uz_AF', '03835-2108', '+1.869.955.9329', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (44, 'Kyle', 'Wisoky', 'sandrine90@koepp.com', 'Apt. 376', '19627 Littel Road Apt. 118\nFraneckifort, PA 93197-3199', 'East Fritz', 'Burkina Faso', 'New Hampshire', 'es_GT', '90083-1548', '+1-206-223-4217', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (45, 'Sasha', 'Koelpin', 'carmine26@murray.com', 'Apt. 134', '9750 Hackett Burgs Suite 154\nJohnsville, AK 20879', 'Port Lauryhaven', 'Angola', 'Vermont', 'sw_TZ', '69783-6673', '856-615-3659', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (46, 'Penelope', 'Gutkowski', 'obruen@hotmail.com', 'Apt. 050', '39811 Corine Forge Suite 112\nJulianneville, GA 13973-1969', 'Hermanshire', 'Austria', 'District of Columbia', 'se_NO', '98811-6417', '(639) 568-5147', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (47, 'Archibald', 'Green', 'king.donnie@kertzmann.com', 'Suite 211', '61565 Maybelle Extension\nLake Desiree, NM 75241', 'Lake Isaias', 'Monaco', 'Washington', 'ar_AE', '25045', '(382) 861-2621 x37529', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (48, 'Meagan', 'Russel', 'wbrown@yahoo.com', 'Suite 909', '76689 Johan Ways Suite 635\nStevestad, KS 14762', 'Elbertside', 'Cyprus', 'South Dakota', 'en_MH', '67420-1606', '510-342-8797', 2, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (49, 'Samir', 'Zieme', 'jacobson.mekhi@hotmail.com', 'Apt. 232', '96541 Ona Haven Apt. 549\nBaumbachshire, MD 69815-3827', 'Abdulhaven', 'Niue', 'Louisiana', 'de_DE', '93286', '1-748-778-1886', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (50, 'Odessa', 'Lesch', 'ajerde@gmail.com', 'Suite 007', '327 Dale Drive Suite 436\nNicoletteport, MT 78750', 'West Arloberg', 'Micronesia', 'Oklahoma', 'gl_ES', '61066-1035', '+1-739-938-6777', 1, '2019-12-17 10:38:05', '2019-12-17 10:38:05');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;

-- Dumping structure for table tispedisco.admins
CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.admins: ~2 rows (approximately)
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
    (1, 'Ferdous Anam', 'anam@gmail.com', '2019-12-17 10:38:05', '$2y$10$iA9zXLZYneXFqpYd/281ieCXIxaWewdTch22NncAsunys7K31/Z3q', 'Profile.png', NULL, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (2, 'Hasan ali', 'hasan@dev.com', '2019-12-17 10:38:05', '$2y$10$vPUEpXfhRc37XdmERH2e9uK/J24/n6YVdbeNRH5Y7cHlJyDPygh9e', 'Profile.png', '4IhvUlLEYHRt6j5J89ko5dByvcNCXl1U834TR7qvsPjFYVRSuf39isV5IF8e', '2019-12-17 10:38:05', '2019-12-17 10:38:05');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;

-- Dumping structure for table tispedisco.admin_role
CREATE TABLE IF NOT EXISTS `admin_role` (
  `admin_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  KEY `admin_role_admin_id_foreign` (`admin_id`),
  KEY `admin_role_role_id_foreign` (`role_id`),
  CONSTRAINT `admin_role_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE,
  CONSTRAINT `admin_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.admin_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_role` ENABLE KEYS */;

-- Dumping structure for table tispedisco.billing_addresses
CREATE TABLE IF NOT EXISTS `billing_addresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pec_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sdi_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `billing_addresses_user_id_foreign` (`user_id`),
  CONSTRAINT `billing_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.billing_addresses: ~0 rows (approximately)
/*!40000 ALTER TABLE `billing_addresses` DISABLE KEYS */;
INSERT INTO `billing_addresses` (`id`, `phone`, `email`, `company_name`, `pec_address`, `sdi_code`, `vat_no`, `address_1`, `address_2`, `city`, `province`, `postcode`, `country`, `user_id`, `created_at`, `updated_at`) VALUES
    (1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2019-12-18 09:14:48', '2019-12-18 09:14:48');
/*!40000 ALTER TABLE `billing_addresses` ENABLE KEYS */;

-- Dumping structure for table tispedisco.carriers
CREATE TABLE IF NOT EXISTS `carriers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `configs` text COLLATE utf8mb4_unicode_ci,
  `fee` float(6,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.carriers: ~1 rows (approximately)
/*!40000 ALTER TABLE `carriers` DISABLE KEYS */;
INSERT INTO `carriers` (`id`, `title`, `logo`, `configs`, `fee`, `created_at`, `updated_at`) VALUES
    (51, 'Fedex', 'uploads/carriers/I6VoKy4rB1sNUchkguup1LUXINPOdHUS6yqV3gjS.png', '{"app_key":"MiU8OafJEivTxizH","account_number":"510087100","password":"QxaSdy7Zd22911ksini5Ldcw6","meter_number":"114028252"}', 2.00, '2019-12-18 11:18:00', '2020-01-02 10:39:54');
/*!40000 ALTER TABLE `carriers` ENABLE KEYS */;

-- Dumping structure for table tispedisco.custom_pages
CREATE TABLE IF NOT EXISTS `custom_pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `page_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `custom_pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.custom_pages: ~0 rows (approximately)
/*!40000 ALTER TABLE `custom_pages` DISABLE KEYS */;
INSERT INTO `custom_pages` (`id`, `page_title`, `status`, `slug`, `meta_title`, `meta_description`, `content`, `created_at`, `updated_at`) VALUES
    (1, 'test', 0, 'test', 'test', 'test', '<p>test</p>', '2019-12-21 12:14:38', '2019-12-21 12:14:38');
/*!40000 ALTER TABLE `custom_pages` ENABLE KEYS */;

-- Dumping structure for table tispedisco.email_resets
CREATE TABLE IF NOT EXISTS `email_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `email_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.email_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `email_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_resets` ENABLE KEYS */;

-- Dumping structure for table tispedisco.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table tispedisco.inner_replies
CREATE TABLE IF NOT EXISTS `inner_replies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reply_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('new','unread','read','replied') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `user_type` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.inner_replies: ~0 rows (approximately)
/*!40000 ALTER TABLE `inner_replies` DISABLE KEYS */;
INSERT INTO `inner_replies` (`id`, `reply_id`, `user_id`, `message`, `file`, `status`, `user_type`, `created_at`, `updated_at`) VALUES
    (1, 1, 2, 'sgfdrtyrtyeg', NULL, 'unread', 'user', '2019-12-17 12:12:27', '2019-12-17 12:12:27');
/*!40000 ALTER TABLE `inner_replies` ENABLE KEYS */;

-- Dumping structure for table tispedisco.invoices
CREATE TABLE IF NOT EXISTS `invoices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `returnCode` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `docId` int(11) NOT NULL,
  `docNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoices_order_id_foreign` (`order_id`),
  CONSTRAINT `invoices_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.invoices: ~0 rows (approximately)
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;

-- Dumping structure for table tispedisco.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.migrations: ~21 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
    (1, '2014_10_12_000000_create_users_table', 1),
    (2, '2014_10_12_100000_create_password_resets_table', 1),
    (3, '2019_07_21_113612_create_email_resets_table', 1),
    (4, '2019_07_27_052806_create_admins_table', 1),
    (5, '2019_08_02_050301_create_addresses_table', 1),
    (6, '2019_08_03_114008_create_pages_table', 1),
    (7, '2019_08_06_070610_create_carriers_table', 1),
    (8, '2019_08_07_093753_create_roles_table', 1),
    (9, '2019_08_07_093928_create_admin_role_table', 1),
    (10, '2019_08_09_090912_create_orders_table', 1),
    (11, '2019_08_19_000000_create_failed_jobs_table', 1),
    (13, '2019_08_23_094316_create_rates_table', 1),
    (14, '2019_08_27_062736_create_invoices_table', 1),
    (15, '2019_11_18_072409_create_order_items_table', 1),
    (16, '2019_11_18_074302_create_tickets_table', 1),
    (17, '2019_11_27_130902_create_profiles_table', 1),
    (18, '2019_11_28_114212_create_billing_addresses_table', 1),
    (19, '2019_11_30_152443_create_custom_pages_table', 1),
    (20, '2019_12_19_095406_create_service_types_table', 2),
    (21, '2019_12_19_095420_create_rate_types_table', 2),
    (22, '2019_12_19_101825_create_packaging_types_table', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table tispedisco.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `itemsequenceno` int(11) DEFAULT NULL,
  `itemtype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_date` date DEFAULT NULL,
  `volume` double NOT NULL,
  `weight` double NOT NULL,
  `length` double NOT NULL,
  `height` double NOT NULL,
  `width` double NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `itemaction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_full_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_pec_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_sdi_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_vat_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_address_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_address_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_full_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_address_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `additional_cost` double DEFAULT NULL,
  `total_cost` double NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `address_id` bigint(20) unsigned DEFAULT NULL,
  `possible_notes` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  KEY `orders_address_id_foreign` (`address_id`),
  CONSTRAINT `orders_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table tispedisco.order_items
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `shipment_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `length` double NOT NULL,
  `height` double NOT NULL,
  `width` double NOT NULL,
  `volume` double NOT NULL,
  `weight` double NOT NULL,
  `price` double NOT NULL,
  `vat` double NOT NULL,
  `additional_cost` double DEFAULT NULL,
  `total_cost` double NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.order_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;

-- Dumping structure for table tispedisco.packaging_types
CREATE TABLE IF NOT EXISTS `packaging_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `carrier_id` bigint(20) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `packaging_types_carrier_id_foreign` (`carrier_id`),
  CONSTRAINT `packaging_types_carrier_id_foreign` FOREIGN KEY (`carrier_id`) REFERENCES `carriers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.packaging_types: ~11 rows (approximately)
/*!40000 ALTER TABLE `packaging_types` DISABLE KEYS */;
INSERT INTO `packaging_types` (`id`, `carrier_id`, `title`, `created_at`, `updated_at`) VALUES
    (1, 51, 'FEDEX_BOX', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (2, 51, 'FEDEX_ENVELOPE', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (3, 51, 'FEDEX_PAK', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (4, 51, 'FEDEX_TUBE', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (5, 51, 'YOUR_PACKAGING', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (8, 51, 'FEDEX_10KG_BOX', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (9, 51, 'FEDEX_25KG_BOX', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (10, 51, 'FEDEX_EXTRA_LARGE_BOX', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (11, 51, 'FEDEX_LARGE_BOX', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (12, 51, 'FEDEX_MEDIUM_BOX', '2019-12-19 10:21:15', '2019-12-19 10:21:16'),
    (13, 51, 'FEDEX_SMALL_BOX', '2019-12-19 10:21:15', '2019-12-19 10:21:16');
/*!40000 ALTER TABLE `packaging_types` ENABLE KEYS */;

-- Dumping structure for table tispedisco.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_title_unique` (`title`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.pages: ~0 rows (approximately)
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table tispedisco.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table tispedisco.profiles
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `address_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isInvoice` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_user_id_foreign` (`user_id`),
  CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.profiles: ~0 rows (approximately)
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` (`id`, `address_1`, `address_2`, `city`, `country`, `state`, `province`, `postcode`, `phone`, `isInvoice`, `user_id`, `created_at`, `updated_at`) VALUES
    (1, 'as', NULL, 'as', 'Saudi Arabia', NULL, 'as', 'as', '01623976787', 0, 2, '2019-12-18 09:14:48', '2019-12-18 09:15:01');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;

-- Dumping structure for table tispedisco.rate_types
CREATE TABLE IF NOT EXISTS `rate_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `carrier_id` bigint(20) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rate_types_carrier_id_foreign` (`carrier_id`),
  CONSTRAINT `rate_types_carrier_id_foreign` FOREIGN KEY (`carrier_id`) REFERENCES `carriers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.rate_types: ~2 rows (approximately)
/*!40000 ALTER TABLE `rate_types` DISABLE KEYS */;
INSERT INTO `rate_types` (`id`, `carrier_id`, `title`, `created_at`, `updated_at`) VALUES
    (1, 51, 'PAYOR_ACCOUNT_SHIPMENT', '2019-12-19 10:06:45', NULL),
    (2, 51, 'PAYOR_LIST_SHIPMENT', '2019-12-19 10:06:45', NULL);
/*!40000 ALTER TABLE `rate_types` ENABLE KEYS */;

-- Dumping structure for table tispedisco.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
    (1, 'Super admin', '2019-12-17 10:38:05', '2019-12-17 10:38:05');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table tispedisco.service_types
CREATE TABLE IF NOT EXISTS `service_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `carrier_id` bigint(20) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_types_carrier_id_foreign` (`carrier_id`),
  CONSTRAINT `service_types_carrier_id_foreign` FOREIGN KEY (`carrier_id`) REFERENCES `carriers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.service_types: ~56 rows (approximately)
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
INSERT INTO `service_types` (`id`, `carrier_id`, `title`, `created_at`, `updated_at`) VALUES
    (1, 51, 'EUROPE_FIRST_INTERNATIONAL_PRIORITY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (2, 51, 'FEDEX_1_DAY_FREIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (3, 51, 'FEDEX_2_DAY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (4, 51, 'FEDEX_2_DAY_AM', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (5, 51, 'FEDEX_2_DAY_FREIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (6, 51, 'FEDEX_3_DAY_FREIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (7, 51, 'FEDEX_CARGO_AIRPORT_TO_AIRPORT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (8, 51, 'FEDEX_CARGO_FREIGHT_FORWARDING', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (9, 51, 'FEDEX_CARGO_INTERNATIONAL_EXPRESS_FREIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (10, 51, 'FEDEX_CARGO_INTERNATIONAL_PREMIUM', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (11, 51, 'FEDEX_CARGO_MAIL', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (12, 51, 'FEDEX_CARGO_REGISTERED_MAIL', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (13, 51, 'FEDEX_CARGO_SURFACE_MAIL', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (14, 51, 'FEDEX_CUSTOM_CRITICAL_AIR_EXPEDITE', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (15, 51, 'FEDEX_CUSTOM_CRITICAL_AIR_EXPEDITE_EXCLUSIVE_USE', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (16, 51, 'FEDEX_CUSTOM_CRITICAL_AIR_EXPEDITE_NETWORK', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (17, 51, 'FEDEX_CUSTOM_CRITICAL_CHARTER_AIR', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (18, 51, 'FEDEX_CUSTOM_CRITICAL_POINT_TO_POINT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (19, 51, 'FEDEX_CUSTOM_CRITICAL_SURFACE_EXPEDITE', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (20, 51, 'FEDEX_CUSTOM_CRITICAL_SURFACE_EXPEDITE_EXCLUSIVE_USE', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (21, 51, 'FEDEX_CUSTOM_CRITICAL_TEMP_ASSURE_AIR', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (22, 51, 'FEDEX_CUSTOM_CRITICAL_TEMP_ASSURE_VALIDATED_AIR', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (23, 51, 'FEDEX_CUSTOM_CRITICAL_WHITE_GLOVE_SERVICES', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (24, 51, 'FEDEX_DISTANCE_DEFERRED', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (25, 51, 'FEDEX_EXPRESS_SAVER', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (26, 51, 'FEDEX_FIRST_FREIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (27, 51, 'FEDEX_FREIGHT_ECONOMY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (28, 51, 'FEDEX_FREIGHT_PRIORITY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (29, 51, 'FEDEX_GROUND', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (30, 51, 'FEDEX_INTERNATIONAL_PRIORITY_PLUS', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (31, 51, 'FEDEX_NEXT_DAY_AFTERNOON', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (32, 51, 'FEDEX_NEXT_DAY_EARLY_MORNING', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (33, 51, 'FEDEX_NEXT_DAY_END_OF_DAY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (34, 51, 'FEDEX_NEXT_DAY_FREIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (35, 51, 'FEDEX_NEXT_DAY_MID_MORNING', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (36, 51, 'FIRST_OVERNIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (37, 51, 'GROUND_HOME_DELIVERY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (38, 51, 'INTERNATIONAL_DISTRIBUTION_FREIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (39, 51, 'INTERNATIONAL_ECONOMY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (40, 51, 'INTERNATIONAL_ECONOMY_DISTRIBUTION', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (41, 51, 'INTERNATIONAL_ECONOMY_FREIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (42, 51, 'INTERNATIONAL_FIRST', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (43, 51, 'INTERNATIONAL_GROUND', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (44, 51, 'INTERNATIONAL_PRIORITY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (45, 51, 'INTERNATIONAL_PRIORITY_DISTRIBUTION', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (46, 51, 'INTERNATIONAL_PRIORITY_EXPRESS', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (47, 51, 'INTERNATIONAL_PRIORITY_FREIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (48, 51, 'PRIORITY_OVERNIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (49, 51, 'SAME_DAY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (50, 51, 'SAME_DAY_CITY', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (51, 51, 'SAME_DAY_METRO_AFTERNOON', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (52, 51, 'SAME_DAY_METRO_MORNING', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (53, 51, 'SAME_DAY_METRO_RUSH', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (54, 51, 'SMART_POST', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (55, 51, 'STANDARD_OVERNIGHT', '2019-12-19 10:48:21', '2019-12-19 10:48:21'),
    (56, 51, 'TRANSBORDER_DISTRIBUTION_CONSOLIDATION', '2019-12-19 10:48:21', '2019-12-19 10:48:21');
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;

-- Dumping structure for table tispedisco.tickets
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('new','unread','read','replied') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `state` enum('open','closed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `is_paralyzes` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.tickets: ~2 rows (approximately)
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` (`id`, `user_id`, `title`, `message`, `file`, `status`, `state`, `is_paralyzes`, `created_at`, `updated_at`) VALUES
    (1, 2, 'First', 'About first message', NULL, 'unread', 'open', 0, '2019-12-17 10:38:05', '2019-12-17 10:38:05'),
    (2, 2, 'First', 'About first message', NULL, 'unread', 'closed', 0, '2019-12-17 10:38:05', '2019-12-17 10:38:05');
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;

-- Dumping structure for table tispedisco.ticket_replies
CREATE TABLE IF NOT EXISTS `ticket_replies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('new','unread','read','replied') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `user_type` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.ticket_replies: ~4 rows (approximately)
/*!40000 ALTER TABLE `ticket_replies` DISABLE KEYS */;
INSERT INTO `ticket_replies` (`id`, `ticket_id`, `user_id`, `message`, `file`, `status`, `user_type`, `created_at`, `updated_at`) VALUES
    (1, 1, 2, 'dhfhfghdfg', NULL, 'unread', 'user', '2019-12-17 12:12:00', '2019-12-17 12:12:00'),
    (2, 1, 2, 'fhfghfgh', NULL, 'unread', 'user', '2019-12-17 12:13:14', '2019-12-17 12:13:14'),
    (3, 1, 2, 'ghdghdghd', NULL, 'unread', 'admin', '2019-12-17 12:20:42', '2019-12-17 12:20:42'),
    (4, 1, 2, 'l;kjhl;jk', NULL, 'unread', 'admin', '2019-12-17 12:21:04', '2019-12-17 12:21:04'),
    (5, 1, 1, 'Testing file upload', 'uploads/file/1576574425-1.jpg', 'unread', 'admin', '2019-12-17 15:20:52', '2019-12-17 15:20:52');
/*!40000 ALTER TABLE `ticket_replies` ENABLE KEYS */;

-- Dumping structure for table tispedisco.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_subscribed` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_customer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tispedisco.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `last_name`, `is_subscribed`, `email`, `email_verified_at`, `password`, `avatar`, `stripe_customer_id`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
    (1, 'Ferdous', 'Anam', 0, 'anam@mail.com', '2019-12-17 10:38:05', '$2y$10$5wye3zGe9dStLIuWYyAYpuW5tXrK8YkfSIfbQ6YIZDgFoVPKca9em', NULL, NULL, NULL, 0, '2019-12-17 10:38:05', '2019-12-17 14:26:40'),
    (2, 'Hasan', 'Mahmud', 0, 'hasan@dev.com', '2019-12-17 10:12:05', '$2y$10$WnHABZivkVNDMxf5y6E3SO/IufkIeqTcg6fLGuz5TOqgRN9S20v2G', NULL, 'cus_GNK0ulQiwrwtwZ', 'MME9gQDrkjO7opRH7DtlUKFxOD4d7JEMv2ZW3lTovA8doXisiLF22PyeqQWJ', 0, '2019-12-17 10:38:05', '2019-12-24 10:26:11'),
    (3, 'Recipient Name', 'dsada', 1, 'recipient@gmail.com', NULL, '$2y$10$bFQJKE2XKTfd4kPiAZQmpewVwtkwSjVz4mC3/h.xmOIvyUYAhS0Vq', NULL, NULL, 'hFQED4p5HKoRwYkzBy5R9ptWkIIppgrqN9gjjCK2QurXiHaZHkqgumVNDaEn', 0, '2019-12-17 17:34:29', '2019-12-17 17:34:29');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
