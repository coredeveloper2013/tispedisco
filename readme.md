# TiSpedisco | A Web Application For A TranService Company 

### Rules for developement & You should maintain 
- Keep the code clean.
- Editorconfig
	`
	indent_style = space
	indent_size = 4
	`
- Dont write big inline css / js, write in custom(js|css) file.
- Dont create resource type route if its not needed.
- Dont create controller for each pages, create for a function.
- Dont overload controller methods by business logic, move them to a service then handle all the operations there and get response


### How to setup
- Install composer packages
    - `composer install`
- Copy `.env.example` file as `.env`
- Update the settings/keys as needed
- Create database in your server
- Configure database details in `.env` file
- Run migration
    - `php artisan migrate`
- Run seeder
    - `php artisan db:seed`
- If everything works correctly, youre done


### Troubleshoting
- If migration didnt work, look for `database.sql` file in root folder.
- Clear your database, then import

### Demo login
- User: http://localhost:8000/login
    - hasan@dev.com
    - 12345678
    
- Admin: http://localhost:8000/secure/administrator/login
    - anam@gmail.com
    - 12345678
    
### Current Order Flow
- Enter Locations
- Select Carriers
- Shipping calculation
- Shipping addresses
- Payment

### Requested Order Flow: due to different cost upon multiple package
- Enter Locations
- Shipping calculation
- Select Carriers
- Shipping addresses
- Payment
