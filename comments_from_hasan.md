### Testing Address
 - From
    60 Cathedral Square, Christchurch Central City, Christchurch 8011, New Zealand
 - To
    Crown Entertainment Complex, 8 Whiteman St, Southbank VIC 3006, Australia



### Notable Bugs

- Home page: `Fixed`
    - http://localhost:8000
    - Order creation form doesnt validate TOS Agreement

- Carrrer List page: `Fixed`
    - http://localhost:8000/ship-comparator
    - Doesnt validates if location exists in session, throws error if opened without `location` session

- Ship Details page: `Fixed`
    - http://localhost:8000/ship-details
    - Doesnt validates past data, like `location` `selected_carrer` 
    - Doesnt validates form data before storing

- Ship Address page: `Fixed`
    - http://localhost:8000/ship-address
    - No validation of past data
    - No error message on the form, until "Invoice Option" is checked
    - No proper validation before storing

- Payment Design Page:
    - http://localhost:8000/payment-design
    - No proper validation before moving forward

- Order confirmation page: 
    - http://localhost:8000/order-confirm
    - Still has bugs with FATTURA24 API
