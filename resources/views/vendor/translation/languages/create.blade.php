@extends('Admin.layouts.app')

@section('page-title')
    {{ __('translations.Carrier API Settings') }}
@endsection


@push('css')
    <link rel="stylesheet" href="/vendor/translation/css/main.css">
@endpush

@section('content')
    <div id="app">
        <div class="panel">

            <div class="panel-header">

                {{ __('translation::translation.add_language') }}

            </div>

            <form action="{{ route('admin.create-language') }}" method="POST">

                <fieldset>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="panel-body p-4">

                        @include('translation::forms.text', ['field' => 'name', 'label' => __('translation::translation.language_name'), ])

                        @include('translation::forms.text', ['field' => 'locale', 'label' => __('translation::translation.locale'), ])

                    </div>

                </fieldset>

                <div class="panel-footer flex flex-row-reverse">

                    <button class="button button-blue">
                        {{ __('translation::translation.save') }}
                    </button>

                </div>

            </form>

        </div>
    </div>
@endsection

@push('scripts')
    <script src="/vendor/translation/js/app.js"></script>
@endpush