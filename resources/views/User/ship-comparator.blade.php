@extends('User.layouts.app')

@section('page_tagline', 'Ship Comparator')

@push('cssLib')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css">
@endpush

@section('content')
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="page-text">{{ __('translations.Risultati della ricerca') }}</div>
                    <div class="page-sub-text">
                        <small>
                            {{ __('translations.Da') }} {{ $locations['location_from']['formatted_address'] }} 
                        </small>
    
                        {{ __('translations.a') }}  -

                        <small>
                            {{ $locations['location_to']['formatted_address'] }}
                        </small>          

                        {{ __('translations.Peso') }}: {{ $locations['weight'] }}kg {{-- Distance: {{ $locations['distance'] }} --}}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-right pt-30 text-sm text-green ">
                        <span><i class="mdi mdi-email-outline"></i></span> &nbsp;&nbsp;
                        <span>{{ __('translations.Condividi questo preventivo') }}</span>
                    </div>
                </div>
            </div>
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="comparator-box">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group margin-0 mb-1 position-relative">
                                                <select name="" id=""
                                                        class="form-control mb-1 profile-input input-gray">
                                                    <option value="">{{ __('translations.Filtra i risultati del preventivo')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group margin-0 position-relative">
                                                <select name="" id=""
                                                        class="form-control profile-input input-gray">
                                                    <option value="">{{ __('translations.Ordina i risultati del preventivoo')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="margin-30"></div>
                        @forelse($services as $service)
                            @foreach($service['RatedShipmentDetails'] as $rate)
                                <div class="company-comparator-box-wrapper">
                                    <div class="box-header"></div>
                                    <div class="box-body">
                                        <div class="box-1">
                                            <div class="c-info">{{ __('translations.preferito')}}</div>
                                            <div class="c-logo">
                                                <img class="ship-comparator-img" src="{{ $carrier->logo }}">
                                            </div>
                                        </div>
                                        <div class="box-2">
                                            <div class="text-xs font-bold">{{ $carrier->title }} {{ $service['ServiceDescription']['Description'] }}</div>
                                            <small><p>{{ Str::title(str_replace('_', ' ', $rate['ShipmentRateDetail']['RateType'])) }}</p></small>
                                            @if(isset($service['DeliveryTimestamp']))
                                                <div>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', str_replace('T', '', $service['DeliveryTimestamp']))->diffForHumans() }}</div>
                                            @else
                                                <div>No time retured</div>
                                            @endif
                                        </div>
                                        <div class="box-3">
                                            <div class="text-xs font-bold">{{ __('translations.Ritirato da casa o dal luogo di lavoro')}}</div>
                                            <div>{{ __('translations.Consegnato a casa o sul luogo di lavoro')}}</div>
                                        </div>
                                        <div class="box-4">
                                            <i class="mdi mdi-printer text-xl text-black"></i>
                                        </div>
                                        <div class="box-4">
                                            <i class="mdi mdi-information-outline text-xl text-black"></i>
                                        </div>
                                        <div class="box-5">
                                            <div class="text-box text-left">
                                                <div class="text-xs font-bold"> {{ $rate['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount'] }}€</div>
                                                <div>{{ $rate['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount'] + number_format((($rate['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount'] * $carrier->fee) / 100 ), 2) }} € Inc. IVA</div>
                                            </div>
                                            <div class="btn-box">
                                                <form action="{{ route('ship-details.store') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="selected_service[name]" value="fedex">
                                                    <input type="hidden" name="selected_service[PackagingType]" value="YOUR_PACKAGING">
                                                    <input type="hidden" name="selected_service[ServiceType]" value="{{ $service['ServiceType'] }}">
                                                    <input type="hidden" name="selected_service[DeliveryTimestamp]" value="{{ $service['DeliveryTimestamp'] }}">
                                                    
                                                    <input type="hidden" name="selected_service[RateType]" value="{{ $rate['ShipmentRateDetail']['RateType'] }}">
                                                    <input type="hidden" name="selected_service[TotalCost]" value="{{ $rate['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount'] + number_format((($rate['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount'] * $carrier->fee) / 100 ), 2) }}">
                                                    <input type="hidden" name="selected_service[CarrierCost]" value="{{ $rate['ShipmentRateDetail']['TotalNetChargeWithDutiesAndTaxes']['Amount'] }}">
                                                    <input type="hidden" name="selected_service[CarrierSurcharges]" value="{{ $rate['ShipmentRateDetail']['TotalSurcharges']['Amount'] }}">
                                                    <button class="btn bg-white text-green btn-c carrier" data-id="">{{ __('translations.Prenota')}}</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="full-box">
                                            <div class="sec-30">
                                               <span style="{{ !isset($service['ratings']) ? 'color: white;' : 'color: #ecd216;' }}">
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                    <i class="mdi mdi-star"></i>
                                                </span>
                                                <span>({{ !isset($service['ratings']) ? 'No' : count($service['ratings']) }} {{ __('translations.recensioni')}})</span>
                                            </div>
                                            <div class="sec-70">
                                                <i class="mdi mdi-bullseye"></i>&nbsp;&nbsp; <span>{{ __('translations.Copertura assicurativa opzionale disponibile fino a un valore di 1.000,00 €.')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @empty
                            <h2>Based on the address information entered, there are no FedEx Express® or FedEx Ground® services available. Please enter new information.</h2>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script>
        $('.carrier').click(function (e) {
            let carrier_id = $(this).attr('data-id');
            localStorage.setItem('carrier_id', carrier_id);
            window.location = "{{ route('ship-details.index') }}";
        });
    </script>
@endpush
