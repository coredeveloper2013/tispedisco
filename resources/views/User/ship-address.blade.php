@extends('User.layouts.app')

@push('cssLib')
@endpush

@section('content')
    <div class="page-content" id="shippingAddressVue">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-text">{{ __('translations.Indirizzi della spedizione') }}</div>
                    <div class="page-sub-text">{{ __('translations.Inserisci gli indirizzi della spezione') }}</div>
                </div>
            </div>
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ship-address">
                            <form action="{{ route('ship-address.store') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="pass-box">
                                            <div class="text-box">
                                                <div class="profile-text-1">{{ __('translations.MITTENTE') }}</div>
                                                <div class="profile-text-2"><span>{{ __('translations.Mi serve la fattura') }}</span>
                                                    <span class="toggle-logo">
                                                        <label class="switch">
                                                            <input type="checkbox" name="isInvoice"
                                                            v-model="isInvoice">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Nome') }}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="Inserisci il nome completo"
                                                                   name="sender[first_name]" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Cognome') }} </label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="Inserisci il cognome "
                                                                   name="sender[last_name]" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="">{{ __('translations.Email') }}</label>
                                                    <input type="email"
                                                           class="form-control input-gray profile-input"
                                                           placeholder="Indirizzo email"
                                                           name="sender[email]" required>
                                                    <br>
                                                    <small class="text-green" style="text-decoration: underline">{{ __('translations.Non hai un account? Crealo qui') }}</small>
                                                </div>
                                            </div>

                                            <div class="row" v-if="isInvoice">
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Nome Società') }}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Scrivi il nome della società') }}"
                                                                   name="sender[company_name]">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Indirizzo PEC') }} </label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Indirizzo PEC') }}"
                                                                   name="sender[pec_address]">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Codice SDI') }}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Codice di interscambio') }}"
                                                                   name="sender[sdi_code]">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Partita iva')}} </label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Partita iva') }}"
                                                                   name="sender[vat_no]">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- Shipping Address -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group custom-p-input margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Numero di cellulare') }}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Scrivi il tuo recapito telefonico') }}"
                                                                   name="sender[phone]" required
                                                                   v-model="sender.phone"
                                                                   @change="setBilling">
                                                        </div>
                                                    </div>
                                                    <div class="custom-p-logo">
                                                        <button class="btn bg-green p-form-logo"><i
                                                                class="mdi mdi-alert-circle-outline"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="Indirizzo">{{ __('translations.Indirizzo') }}</label>
                                                    <input type="text" id="Indirizzo"
                                                           class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Indirizzo riga 1') }}"
                                                           name="sender[address_1]" required
                                                           v-model="sender.address_1"
                                                           @change="setBilling">
                                                </div>
                                            </div>

                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <input type="text" class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Indirizzo riga 2') }}"
                                                           name="sender[address_2]"
                                                           v-model="sender.address_2"
                                                           @change="setBilling">
                                                </div>
                                            </div>

                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="">{{ __('translations.Città')}}</label>
                                                    <input type="text" class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Seleziona la città') }}"
                                                           name="sender[city]" required
                                                           v-model="sender.city"
                                                           @change="setBilling">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Provincia') }}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Seleziona la provincia') }}"
                                                                   name="sender[province]" required
                                                                   v-model="sender.province"
                                                                   @change="setBilling">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Cap') }}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Seleziona il cap') }}"
                                                                   name="sender[post_code]" required
                                                                   v-model="sender.post_code"
                                                                   @change="setBilling">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="">{{ __('translations.Paese')}}</label>
                                                    <input type="text" class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Seleziona il paese')}}"
                                                           name="sender[country]" required
                                                           v-model="sender.country"
                                                           @change="setBilling">
                                                </div>
                                            </div>
                                            <!-- END Shipping Address -->

                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <input class="form-check-input styled-checkbox" type="checkbox"
                                                           name="sameBilling"
                                                           id="check"  checked
                                                           v-model="sameBilling">
                                                    <label for="check" class="form-check-label text-black">
                                                        <span class="font-400">
                                                            {{ __('translations.L\'ndirizzo del mittente è diverso dall\'indirizzo di partenza della spedizione')}}
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>


                                            <!-- Billing Address
                                            <section>
                                              <div class="row" v-if="isInvoice">
                                                <div class="form-group margin-btm-input-lg">
                                                    <div class="mb-1">
                                                        <label for="Indirizzo">Indirizzo</label>
                                                        <input type="text" id="Indirizzo"
                                                               class="form-control input-gray profile-input"
                                                               placeholder="Indirizzo riga 1"
                                                               name="address_1" required
                                                               :readonly="sameBilling"
                                                               v-model="billing.address_1">
                                                    </div>
                                                </div>

                                                <div class="form-group margin-btm-input-lg">
                                                    <div class="mb-1">
                                                        <input type="text" class="form-control input-gray profile-input"
                                                               placeholder="Indirizzo riga 2"
                                                               name="address_2"
                                                               :readonly="sameBilling"
                                                               v-model="billing.address_2">
                                                    </div>
                                                </div>

                                                <div class="form-group margin-btm-input-lg">
                                                    <div class="mb-1">
                                                        <label for="">Città</label>
                                                        <input type="text" class="form-control input-gray profile-input"
                                                               placeholder="Seleziona la città"
                                                               name="city" required
                                                               :readonly="sameBilling"
                                                               v-model="billing.city">
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group margin-btm-input-lg">
                                                            <div class="mb-1">
                                                                <label for="">Provincia</label>
                                                                <input type="text"
                                                                       class="form-control input-gray profile-input"
                                                                       placeholder="Seleziona la provincia"
                                                                       name="province" required
                                                                       :readonly="sameBilling"
                                                                       v-model="billing.province">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group margin-btm-input-lg">
                                                            <div class="mb-1">
                                                                <label for="">Cap</label>
                                                                <input type="text"
                                                                       class="form-control input-gray profile-input"
                                                                       placeholder="Seleziona il cap"
                                                                       name="post_code" required
                                                                       :readonly="sameBilling"
                                                                       v-model="billing.post_code">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group margin-btm-input-lg">
                                                    <div class="mb-1">
                                                        <label for="">Paese</label>
                                                        <input type="text" class="form-control input-gray profile-input"
                                                               placeholder="Seleziona il paese"
                                                               name="country" required
                                                               :readonly="sameBilling"
                                                               v-model="billing.country">
                                                    </div>
                                                </div>
                                                </div>
                                            </section>
                                            <!-- END Billing Address -->

                                        <div v-if="sameBilling">
                                            <div class="form-group margin-btm-input-lg">
                                                <div class="form-divider"></div>
                                            </div>
                                            <div class="text-box">
                                                <div class="profile-text-1">{{ __('translations.MITTENTE: indirizzo di parenza')}}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group custom-p-input margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Numero di cellulare')}}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Scrivi il tuo recapito telefonico')}}"
                                                                   name="departure_phone" required>
                                                        </div>
                                                    </div>
                                                    <div class="custom-p-logo">
                                                        <button class="btn bg-green p-form-logo"><i
                                                                class="mdi mdi-alert-circle-outline"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="Indirizzo">{{ __('translations.Indirizzo')}}</label>
                                                    <input type="text" id="Indirizzo"
                                                           class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Indirizzo riga 1')}}"
                                                           name="departure_address_1" required>
                                                </div>
                                            </div>

                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <input type="text" class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Indirizzo riga 2')}}"
                                                           name="departure_address_2">
                                                </div>
                                            </div>

                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="">{{ __('translations.Città')}}</label>
                                                    <input type="text" class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Seleziona la città')}}"
                                                           name="departure_city" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Provincia')}}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Seleziona la provincia')}}"
                                                                   name="departure_province" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Cap')}}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Seleziona il cap')}}"
                                                                   name="departure_post_code" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="">{{ __('translations.Paese')}}</label>
                                                    <input type="text" class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Seleziona il paese')}}"
                                                           name="departure_country" required>
                                                </div>
                                            </div>
                                            <div class="form-group margin-btm-input-lg"></div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="pass-box">
                                            <div class="text-box">
                                                <div class="profile-text-1">{{ __('translations.DESTINATARIO')}}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Nome')}}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Inserisci il nome completo')}}"
                                                                   name="recipient[first_name]" v-model="recipient.first_name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Cognome')}} </label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Inserisci il cognome')}} "
                                                                   name="recipient[last_name]" v-model="recipient.last_name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="">{{ __('translations.Email')}}</label>
                                                    <input type="email"
                                                           class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Indirizzo email')}}"
                                                           name="recipient[email]" v-model="recipient.email" required>
                                                </div>
                                            </div>
                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="Indirizzo">{{ __('translations.Indirizzo')}}</label>
                                                    <input type="text" id="Indirizzo"
                                                           class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Indirizzo riga 1')}}"
                                                           name="recipient[address_1]" v-model="recipient.address_1" required>
                                                </div>
                                            </div>

                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <input type="text" class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Indirizzo riga 2')}}"
                                                           name="recipient[address_2]">
                                                </div>
                                            </div>

                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="">{{ __('translations.Città')}}</label>
                                                    <input type="text" class="form-control input-gray profile-input"
                                                           placeholder="{{ __('translations.Seleziona la città')}}"
                                                           name="recipient[city]" v-model="recipient.city" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Provincia')}}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Seleziona la provincia')}}"
                                                                   name="recipient[province]" v-model="recipient.province" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group margin-btm-input-lg">
                                                        <div class="mb-1">
                                                            <label for="">{{ __('translations.Cap')}}</label>
                                                            <input type="text"
                                                                   class="form-control input-gray profile-input"
                                                                   placeholder="{{ __('translations.Seleziona il cap')}}"
                                                                   name="recipient[post_code]" v-model="recipient.post_code" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="">{{ __('translations.Paese')}}</label>
                                                    <input type="text" class="form-control input-gray profile-input"
                                                           name="recipient[country]" v-model="recipient.country"
                                                           placeholder="{{ __('translations.Seleziona il paese')}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-30"></div>
                                        <div class="margin-30"></div>
                                        <div class="form-group text-right">
                                            <button class="btn btn-success btn-padding-65">{{ __('translations.Continua')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script>
        let fromAddress = "{{ session('locations')['location_from']['formatted_address'] }}".split(',');
        let toAddress = "{{ session('locations')['location_to']['formatted_address'] }}".split(',');

        let vm = new Vue({
            el: '#shippingAddressVue',
            data: {
                sameBilling: false,
                isInvoice: true,
                isDeparture: false,
                sender: {
                    first_name : '',
                    last_name : '',
                    email: '',
                    phone: '',
                    address_1: fromAddress.slice(0, fromAddress.length - 2),
                    address_2: '',
                    city: '{{ session('locations')['fromCity'] }}',
                    province: '',
                    post_code: '{{ session('locations')['fromPostalCode'] }}',
                    country: fromAddress.slice(-1)[0],
                },
                recipient: {
                    first_name : '',
                    last_name : '',
                    email: '',
                    phone: '',
                    address_1: toAddress.slice(0, toAddress.length - 2),
                    address_2: '',
                    city: '{{ session('locations')['toCity'] }}',
                    province: '',
                    post_code: '{{ session('locations')['toPostalCode'] }}',
                    country: toAddress.slice(-1)[0],
                },
                billing: {
                    first_name : '',
                    last_name : '',
                    email: '',
                    phone: '',
                    address_1: '',
                    address_2: '',
                    city: '',
                    province: '',
                    post_code: '',
                    country: '',
                },
            },
            methods: {
                setBilling() {
                    if (this.sameBilling) {
                        this.billing.phone = this.sender.phone;
                        this.billing.address_1 = this.sender.address_1;
                        this.billing.address_2 = this.sender.address_2;
                        this.billing.province = this.sender.province;
                        this.billing.city = this.sender.city;
                        this.billing.post_code = this.sender.post_code;
                        this.billing.country = this.sender.country;
                    }
                }
            },
            mounted() {
            },
        });

    </script>
@endpush
