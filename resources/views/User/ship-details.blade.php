@extends('User.layouts.app')

@section('page_tagline', 'Calcolo della spedizione')

@push('cssLib')
@endpush

@section('content')
    <div id="app-ship">
        <div class="page-content paddint-top-5">
            <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        {{--alert--}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="page-text">{{ __('translations.Calcolo della spedizione') }}</div>
                        <div class="page-sub-text">{{ __('translations.Fornisci ulteriori dettagli per avere un prezzo della spedizione accurata') }}</div>
                    </div>
                </div>
                <div class="content-wrapper">
                    <div class="col-12">
                        <h4>Your cost: $<span id="totalCost">@{{ totalCost }}</span></h4>
                    </div>

                    <form action="{{ route('api.ship-details.store') }}" method="post">
                        @csrf
                        <section v-for="(package,index) in shipmentInfo.packages" class="ship-name">
                            <div class="row" v-if="index == 0">
                                <div class="col-md-9">
                                    <div class="text-xl">
                                        <strong>
                                            <input type="text" class="form-control w-100 text-xl text-black font-bold ship-detail-input-1" placeholder="{{ __('translations.Assegna un nome alla spedizione') }}" v-model="package.name" required>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="text-xs mb-1"><strong> {{ __('translations.Data per il ritiro') }}</strong></div>
                                    <div class="position-relative">
                                        <div class="form-group">
                                            <input class="form-control input-gray check_in" type="text" name="collection_date" v-model="shipmentInfo.collection_date" required>
                                        </div>
                                        <span class="calender-down"><i class="mdi mdi-chevron-down"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-divider" v-if="index != 0"></div>
                            <div class="row" v-if="index != 0">
                                <div class="margin-30"></div>
                                <div class="col-md-12">
                                    <input type="text" class="form-control w-100 text-xl text-ash font-bold ship-detail-input-1" placeholder="{{ __('translations.Assegna un nome alla spedizione') }}" v-model="package.name" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="margin-30"></div>
                                <div class="col-md-3">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="">{{ __('translations.Dimensioni') }}</label>
                                            <input type="number" step="0.1" class="form-control input-gray profile-input" placeholder="{{ __('translations.Larghezza') }}" name="width[]" v-model="package.width" required>
                                        </div>
                                    </div>
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="">{{ __('translations.Length') }}</label>
                                            <input type="number" step="0.1" class="form-control input-gray profile-input" placeholder="{{ __('translations.Lunghezza') }}" name="length[]" v-model="package.length" required>
                                        </div>
                                    </div>
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="">{{ __('translations.Valore della merce') }}</label>
                                            <input type="number" step="0.1" class="form-control input-gray profile-input" placeholder="€" name="amount[]" v-model="package.value" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="">{{ __('translations.Height') }}</label>
                                            <input type="number" step="0.1" class="form-control input-gray profile-input" placeholder="{{ __('translations.Altezza') }}" name="height[]" v-model="package.height" required>
                                        </div>
                                    </div>
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="">{{ __('translations.Weight') }}</label>
                                            <input type="number" step="0.1" class="form-control input-gray profile-input" placeholder="{{ __('translations.Peso') }}" name="weight[]" v-model="package.weight" required>
                                        </div>
                                    </div>
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="">{{ __('translations.Servizi aggiuntivi') }}</label>
                                            <select class="form-control custom-select input-gray profile-input" name="additional_service[]" id="" v-model="package.additional_cost" required>
                                                <option value="5">{{ __('translations.Assicurazione') }} - 5€</option>
                                                <option value="5">{{ __('translations.Servzio') }} 2 - 5€</option>
                                                <option value="5">{{ __('translations.Servzio') }} 3 - 5€</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="">{{ __('translations.Contenuto') }}</label>
                                            <textarea name="comment[]" id="" cols="30" rows="9" class="form-control custom-select input-gray profile-input" placeholder="{{ __('translations.Descrivi il contenuto che desideri spedire') }}" v-model="package.comment"></textarea>
                                            <small class="text-ash"><a href="">*{{ __('translations.Consulta le disposizioni riguardanti le restrizioni') }}</a></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 text-center">
                                    <div class="btn-wrapper">
                                        <button type="button" class="btn p-form-logo bg-green" @click="DuplicateShipment(package)">
                                            <i class="mdi mdi-checkbox-multiple-blank-outline"></i>
                                        </button>
                                        <button type="button" class="btn p-form-logo bg-green" @click="DeleteShipment(index)" v-if="index != 0">
                                            <i class="mdi mdi-close"></i>
                                        </button>
                                        <button type="button" class="btn p-form-logo bg-green" @click="AddNewShipment">
                                            <i class="mdi mdi-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="add-row"></section>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="margin-30"></div>
                                <button :disabled="clicked" :class="isClicked" type="button" @click="updateCost" class="btn btn-primary">{{ __('translations.Update Cost') }}</button>
                                <button :disabled="clicked" type="button" @click="submitShipment" class="btn btn-success btn-padding-65">{{ __('translations.Continua') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://unpkg.com/vuelidate@0.7.4/dist/vuelidate.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/vuelidate@0.7.4/dist/validators.min.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script>
        $('.comment-wrapper').on('click', '.mdi-checkbox-multiple-blank-outline', function (e) {
            e.preventDefault();
            let html = '<section class="ship-name">';
            html += $(this).closest('.ship-name').html();
            html += '</section>';
            $('#add-row').append(html);
        })
    </script>

    <script>
        Vue.use(window.vuelidate.default);
        const { required } = window.validators;

        new Vue({
            el: '#app-ship',
            data: function() {
                return {
                    clicked: false,
                    loadingClass: 0,
                    totalCost: "{{ session('selected_service')['TotalCost'] }}",
                    shipmentInfo: {
                        collection_date: '{{ \Carbon\Carbon::parse(session('selected_service')['DeliveryTimestamp'])->format("Y-m-d") }}',
                        delivery_timestamp: '{{ \Carbon\Carbon::parse(session('selected_service')['DeliveryTimestamp'])->format("Y-m-d") }}',
                        name: '',
                        total_comment: 'Enter your notes about the package',
                        packages: [{
                            name: '',
                            width: 1,
                            height: 1,
                            length: 1,
                            weight: 1,
                            value: 0,
                            additional_cost: '',
                            comment: ''
                        }]
                    }
                }
            },
            computed: {
                isClicked: function () {
                    if(this.loadingClass === 1) {
                        this.clicked = true;
                        return 'btn-has-spinner';
                    } else {
                        return '';
                    }
                }
            },
            methods: {
                DuplicateShipment(package) {
                    this.shipmentInfo.packages.push({
                        name: package.name,
                        width: package.width,
                        height: package.height,
                        length: package.length,
                        weight: package.weight,
                        value: package.value,
                        additional_cost: package.additional_cost,
                        comment: package.comment,
                    })
                },
                DeleteShipment(index) {
                    this.shipmentInfo.packages.splice(index, 1);
                },
                AddNewShipment() {
                    this.shipmentInfo.packages.push({
                        name: '',
                        width: '',
                        height: '',
                        length: '',
                        weight: '',
                        value: '',
                        additional_cost: '',
                        comment: '',
                    })
                },
                updateCost(){
                    let _this = this;
                    this.loadingClass = 1;
                    this.clicked = true;

                    $.ajax({
                        url: "{{ route('ship-details.updateCost') }}",
                        type: "post",
                        data: {
                            packages: this.shipmentInfo.packages,
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(response) {
                            if(response.status == true){
                                _this.totalCost = response.cost;
                                _this.loadingClass = 0;
                                _this.clicked = false;
                            }
                        }
                    })

                },
                submitShipment() {
                    let self = this;
                    $.ajax({
                        url: '{{ route('api.ship-details.index') }}',
                        type: 'post',
                        data: {
                            shipmentInfo: self.shipmentInfo,
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(res) {
                            window.location = "{{ route('ship-address.index') }}";
                        }
                    })
                },
            },
            mounted() {
                let self = this;
                $(".check_in").flatpickr({
                    defaultDate: self.delivery_timestamp,
                    minDate: 'today',
                    altInput: true,
                    altFormat: 'F j, Y',
                    disableMobile: "true",
                    onChange: function(selectedDates, dateStr, instance) {
                        self.collection_date = dateStr;
                    }
                });
            }
        })
    </script>
@endpush
