@extends('User.profile.profileMaster')

@section('userContent')
    <div id="profileVue">
        <div class="state-portfolio">
            <div class="row">
                <form action="{{ route('user.profile.update', $user->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="col-md-6 mb-5">
                        <div class="pass-box">
                            <div class="text-box">
                                <div class="profile-text-1">{{ __('translations.Informazioni account') }}</div>
                                <div class="profile-text-2"><span>{{ __('translations.Mi serve la fattura') }} </span>
                                    <span class="toggle-logo">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="first_name">{{ __('translations.Nome') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="first_name"
                                                   value="{{ old('first_name', $user->first_name) }}"
                                                   placeholder="Scegli una password">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="last_name">{{ __('translations.Cognome') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="last_name"
                                                   value="{{ old('last_name', $user->last_name) }}"
                                                   placeholder="Ripeti la password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group custom-p-input margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="profile_phone">{{ __('translations.Numero di cellulare') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="profile_phone"
                                                   value="{{ old('profile_phone', $user->profile->phone ?? '') }}"
                                                   placeholder="Scrivi il tuo recapito telefonico">
                                        </div>
                                    </div>
                                    <div class="custom-p-logo">
                                        <button class="btn bg-green p-form-logo"><i
                                                class="mdi mdi-alert-circle-outline"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-btm-input-lg">
                                <div class="mb-1">
                                    <label for="profile_address_1">{{ __('translations.Indirizzo') }}</label>
                                    <input type="text" id="Indirizzo" class="form-control input-gray profile-input"
                                           name="profile_address_1"
                                           value="{{ old('profile_address_1', $user->profile->address_1 ?? '') }}"
                                           placeholder="Indirizzo riga 1">
                                </div>
                            </div>

                            <div class="form-group margin-btm-input-lg">
                                <div class="mb-1">
                                    <input type="text" class="form-control input-gray profile-input"
                                           name="profile_address_2"
                                           value="{{ old('profile_address_2', $user->profile->address_2 ?? '') }}"
                                           placeholder="Indirizzo riga 2">
                                </div>
                            </div>

                            <div class="form-group margin-btm-input-lg">
                                <div class="mb-1">
                                    <label for="profile_city">{{ __('translations.Città') }}</label>
                                    <input type="text" class="form-control input-gray profile-input"
                                           name="profile_city"
                                           value="{{ old('profile_city', $user->profile->city ?? '') }}"
                                           placeholder="Seleziona la città">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="profile_province">{{ __('translations.Provincia') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="profile_province"
                                                   value="{{ old('profile_province', $user->profile->province ?? '') }}"
                                                   placeholder="Seleziona la provincia">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="profile_postcode">{{ __('translations.Cap') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="profile_postcode"
                                                   value="{{ old('profile_postcode', $user->profile->postcode ?? '') }}"
                                                   placeholder="Seleziona il cap">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-btm-input-lg">
                                <div class="mb-1">
                                    <label for="profile_country">{{ __('translations.Paese') }}</label>
                                    <input type="text" class="form-control input-gray profile-input"
                                           name="profile_country"
                                           value="{{ old('profile_country', $user->profile->country ?? '') }}"
                                           placeholder="Seleziona il paese">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-5">
                        <div class="pass-box">
                            <div class="text-box">
                                <div class="profile-text-1">{{ __('translations.indirizzo per la fatturazione') }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="company_name">{{ __('translations.Nome Società') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="company_name"
                                                   value="{{ old('company_name', $user->billingAddress->company_name ?? '') }}"
                                                   placeholder="Scrivi il nome della società">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="pec_address">{{ __('translations.Indirizzo PEC') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="pec_address"
                                                   value="{{ old('pec_address', $user->billingAddress->pec_address ?? '') }}"
                                                   placeholder="Indirizzo PEC">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="sdi_code">{{ __('translations.Codice SDI') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="sdi_code"
                                                   value="{{ old('sdi_code', $user->billingAddress->sdi_code ?? '') }}"
                                                   placeholder="{{ __('translations.Codice di interscambio') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="vat_no">{{ __('translations.Partita iva') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="vat_no"
                                                   value="{{ old('vat_no', $user->billingAddress->vat_no ?? '') }}"
                                                   placeholder="{{ __('translations.Partita iva') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group custom-p-input margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="phone">{{ __('translations.Numero di cellulare') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="phone"
                                                   value="{{ old('phone', $user->billingAddress->phone ?? '') }}"
                                                   placeholder="{{ __('translations.Scrivi il tuo recapito telefonico') }}">
                                        </div>
                                    </div>
                                    <div class="custom-p-logo">
                                        <button class="btn bg-green p-form-logo"><i
                                                class="mdi mdi-alert-circle-outline"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-btm-input-lg">
                                <div class="mb-1">
                                    <label for="address_1">{{ __('translations.Indirizzo') }}</label>
                                    <input type="text" id="Indirizzo" class="form-control input-gray profile-input"
                                           name="address_1"
                                           value="{{ old('address_1', $user->billingAddress->address_1 ?? '') }}"
                                           placeholder="{{ __('translations.Indirizzo riga 1') }}">
                                </div>
                            </div>

                            <div class="form-group margin-btm-input-lg">
                                <div class="mb-1">
                                    <input type="text" class="form-control input-gray profile-input"
                                           name="address_2"
                                           value="{{ old('address_2', $user->billingAddress->address_2 ?? '') }}"
                                           placeholder="{{ __('translations.Indirizzo riga 2') }}">
                                </div>
                            </div>

                            <div class="form-group margin-btm-input-lg">
                                <div class="mb-1">
                                    <label for="city">Città</label>
                                    <input type="text" class="form-control input-gray profile-input"
                                           name="city"
                                           value="{{ old('city', $user->billingAddress->city ?? '') }}"
                                           placeholder="{{ __('translations.Seleziona la città') }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="province">Provincia</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="province"
                                                   value="{{ old('province', $user->billingAddress->province ?? '') }}"
                                                   placeholder="{{ __('translations.Seleziona la provincia') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group margin-btm-input-lg">
                                        <div class="mb-1">
                                            <label for="postcode">{{ __('translations.Cap') }}</label>
                                            <input type="text" class="form-control input-gray profile-input"
                                                   name="postcode"
                                                   value="{{ old('postcode', $user->billingAddress->postcode ?? '') }}"
                                                   placeholder="{{ __('translations.Seleziona il cap') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-btm-input-lg">
                                <div class="mb-1">
                                    <label for="country">{{ __('translations.Paese') }}</label>
                                    <input type="text" class="form-control input-gray profile-input"
                                           name="country"
                                           value="{{ old('country', $user->billingAddress->country ?? '') }}"
                                           placeholder="{{ __('translations.Seleziona il paese') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="margin-30"></div>
                        <div class="margin-30"></div>
                        <div class="text-right">
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success btn-padding-65">{{ __('translations.Salva modifiche') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        new Vue({
            el: '#profileVue',
            data: {
                ticket: '',
                rangeValue: 0
            },
            methods: {
                activityChange(activity = 'list') {
                    this.$set(this, 'activity', activity);
                },
                ticketCru(action) {
                    var that = this;
                    var formData = {
                        action: action,
                        ticket: this.ticket
                    }
                    axios.post("{{route('cru.ticket')}}", formData)
                        .then(function (response) {
                            if (!response.data.success) return;
                            switch (action) {
                                case 'create':
                                    that.tickets.push(response.data.ticket);
                                    that.$set(that, 'activity', 'list');
                                    that.clear();
                                    swal({
                                        icon: 'success',
                                        title: 'Created',
                                        text: 'Successfully created a new ticket!',
                                        timer: 1000
                                    })
                                    break;
                                case 'update':
                                    that.$set(that.categories, that.categories.findIndex(c => c.id === that.categoryId), response.data.category);
                                    break;
                                case 'delete':
                                    var tempCategory = that.categories.filter(category => category.id !== that.categoryId);
                                    that.$set(that, 'categories', tempCategory);
                                    break;
                            }
                        })
                        .catch(function (response) {
                            // console.log(response.data.errors);
                        });
                },

                clear() {
                }
            },
            mounted() {
                this.clear();
            },
        });
    </script>
@endpush
