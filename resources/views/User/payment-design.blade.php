@extends('User.layouts.app')

@section('page_tagline', 'Indirizzi della spedizione')

@push('cssLib')
@endpush

@section('content')
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-text">{{ __('translations.Indirizzi della spedizione') }}</div>
                    <div class="page-sub-text">{{ __('translations.Inserisci gli indirizzi della spezione') }}</div>
                </div>
            </div>
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ship-address payment">
                            <form action="{{ route('invoice.store') }}" method="post" id="invoiceStoreForm">
                                @csrf
                                <input type="hidden" name="stripe_publishable_key" value="{{ env('STRIPE_PUBLISHABLE_KEY') }}">
                                <input type="hidden" name="total" value="{{ session('selected_service.TotalCost') }}">
                                <input type="hidden" name="token">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="pass-box">
                                            <div class="text-box">
                                                <div class="profile-text-1">{{ __('translations.totale della spedizione') }}</div>
                                            </div>
                                            <table class="table table-theme">
                                                <thead>
                                                    <tr>
                                                        <td>{{ __('translations.Denominazione spedizione') }}</td>
                                                        <td>{{ __('translations.Costo') }}</td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><strong>{{ __('translations.TOTALE') }}</strong></td>
                                                        <td><strong>{{ session('selected_service.TotalCost') }} €</strong></td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <div class="form-group margin-btm-input-lg">
                                                <div class="mb-1">
                                                    <label for="">{{ __('translations.Eventuali note') }}</label>
                                                    <textarea name="possible_notes" class="form-control input-gray profile-input" id="" cols="30" rows="10" placeholder="{{ __('translations.Scrivi eventuali note che vuoi comunicare allo spedizioniere') }}"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="pass-box">
                                            <div class="text-box">
                                                <div class="profile-text-1">{{ __('translations.Pagamento') }}</div>
                                            </div>
                                            <div class="text-box">
                                                <div class="">{{ __('translations.Inserisci i dati della carta di credito per procedere alla conferma del pagamento.') }}</div>
                                            </div>
                                            <div class="margin-30"></div>
                                            <div class="payment-method-wrapper">
                                                <div class="sec-left">
                                                    <div class="form-group">
                                                        <input class="form-check-input styled-checkbox-round"
                                                               type="radio" value="1"
                                                               name="payment_method"
                                                               id="check-input1" required disabled>
                                                        <label class="form-check-label text-black" for="check-input1">
                                                            <span class="font-400">{{ __('translations.Paga con Paypa') }}l <br>(not available right now)</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="sec-right text-center">
                                                    <img src="{{asset('images/home-img/paypal-logo.png') }}" alt="">
                                                </div>
                                            </div>
                                            <div class="payment-method-wrapper">
                                                <div class="sec-left">
                                                    <div class="form-group">
                                                        <input class="form-check-input styled-checkbox-round" checked
                                                               type="radio" value="2"
                                                               name="payment_method"
                                                               id="check-input2" required>
                                                        <label class="form-check-label text-black" for="check-input2">
                                                            <span class="font-400">{{ __('translations.Paga con Carta di Credito') }}</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="sec-right text-center">
                                                    <img class="payment-logo"
                                                         src="{{asset('images/home-img/payments.png') }}" alt="">
                                                </div>
                                            </div>
                                            <div>

                                            </div>
                                                <div class="form-group position-relative margin-btm-input-lg">
                                                    <div class="mb-1">
                                                        <label for="">{{ __('translations.Numero carta di credito') }}</label>
                                                        <input type="text" class="form-control input-gray profile-input"
                                                                placeholder="0000 0000 0000 0000"
                                                                name="credit_card_no" required>
                                                        <img class="input-img" src="{{asset('images/home-img/cards.png') }}"
                                                                alt="">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group margin-btm-input-lg">
                                                            <div class="mb-1">
                                                                <label for="">{{ __('translations.Data di scadenza') }}</label>
                                                                <input type="text"
                                                                        class="form-control input-gray profile-input"
                                                                        placeholder="MM"
                                                                        name="card_exp_month" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group margin-btm-input-lg">
                                                            <div class="mb-1">
                                                                <label for="">{{ __('translations.Data di scadenza') }}</label>
                                                                <input type="text"
                                                                        class="form-control input-gray profile-input"
                                                                        placeholder="YY"
                                                                        name="card_exp_year" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group margin-btm-input-lg">
                                                            <div class="mb-1">
                                                                <label for="">CVC</label>
                                                                <input type="text"
                                                                        class="form-control input-gray profile-input"
                                                                        placeholder="000"
                                                                        name="card_cvc" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="margin-30"></div>
                                        <div class="form-group">
                                            <input class="form-check-input styled-checkbox" type="checkbox" value="1"
                                                   name="isAccepted"
                                                   id="check" required>
                                            <label for="check"></label>
                                            <label class="form-check-label text-black">
                                                <span class="font-400"> {{ __('translations.Accetto le condizioni di vendita espresse nei termini e condizioni del sito web') }}</span>
                                            </label>
                                        </div>
                                        <div class="margin-30"></div>
                                        <div class="margin-30"></div>
                                        <div class="form-group text-right">
                                            <button class="btn btn-success btn-padding-65" id="orderSubmit" style="display:none">{{ __('translations.Conferma pagamento') }}</button>
                                            <button type="button" class="btn btn-success btn-padding-65" onclick="getToken()" id="paymentValidate">{{ __('translations.Convalida la carta') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <!-- Scripts for stripe: -->
    <script src="https://js.stripe.com/v2/"></script>
    <script>
        function getToken() {
            Stripe.setPublishableKey($('input[name=stripe_publishable_key]').val());
            let card = {
                number: $('input[name="credit_card_no"]').val(),
                cvc: $('input[name="card_cvc"].cvc').val(),
                exp_month: $('input[name="card_exp_month"]').val(),
                exp_year: $('input[name="card_exp_year"]').val()
            };
            Stripe.card.createToken(card, function(status, response) {
                let $form = $('#payment-form');
                if (response.error) {
                    alert('card invalid');
                } else {
                    // Get the token ID:
                    let token = response.id;
                    $('#paymentValidate').hide();
                    $('#orderSubmit').show();
                    $('input[name="token"]').val(token)
                    $('#invoiceStoreForm').submit();
                }
            });
        }
    </script>
@endpush
