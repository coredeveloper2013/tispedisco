@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    {{ __('translations.Add new carrier settings') }}
@endsection

@push('cssLib')
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Add new carrier settings') }}</a></li>
@endsection

@section('content')
<div class="page-content container-fluid">
    <form method="post" action="{{ route('admin.carriers.store') }}" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        <div class="row">
            <div class="col-md-4">
                <div class="from-group">
                    <label>{{ __('translations.Carrier Title') }}</label>
                    <input class="form-control" type="text" name="title" required>
                </div>
            </div>
            <div class="col-md-4">
                <label>{{ __('translations.Carrier Logo') }}</label>
                 <div class="custom-file">
                    <input name="logo" type="file" class="custom-file-input" required>
                    <label class="custom-file-label">{{ __('translations.Choose file') }}...</label>
                    @if($errors->has('logo'))
                        <div class="invalid-feedback">{{ $errors->first('logo') }}</div>
                    @endif
                  </div>
            </div>
            <div class="col-md-4">
                <label>{{ __('translations.Carrier Fee') }}(in %)</label>
                <input name="fee" type="number" step="0.01" class="form-control" required>
            </div>
        </div>
        <hr>
        <div class="row text-right">
            <div class="col-12"><button id="addKey" class="btn btn-primary">{{ __('translations.Add New Key') }}</button></div>
        </div>
        <hr>
        <div class="row" id="keys">
            <div class="col-md-3 mt-4">
                <div class="row">
                    <div class="col-6">
                        <label>{{ __('translations.Key 1') }}</label>
                    </div>
                    <div class="col-6">
                        <button type="button" id="removeKey" style="padding: 2px 5px;" class="btn btn-danger pull-right"><i class="fa fa-trash-alt"></i></button>
                    </div>
                </div>
                <div class="from-group">
                    <input class="form-control" type="text" name="configs[1][name]" value="" placeholder="Enter key name" required>
                </div>
                <div class="from-group mt-2">
                    <input class="form-control" type="text" name="configs[1][value]" value="" placeholder="Enter key value" required>
                </div>
            </div>
        </div>
        <div class="row mt-3">
             <div class="col-12">
                 <button class="btn btn-success">{{ __('translations.Save Settings') }}</button>
             </div>
         </div> 
    </form>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        let keys = 2;

        $("#addKey").click(function(e) {
            e.preventDefault();
            
            $("#keys").append(`<div class="col-md-3 mt-4">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Key ${keys}</label>
                        </div>
                    </div>
                    <div class="from-group">
                        <input class="form-control" type="text" name="configs[${keys}][name]" value="" placeholder="Enter key name" required>
                    </div>
                    <div class="from-group mt-2">
                        <input class="form-control" type="text" name="configs[${keys}][value]" value="" placeholder="Enter key value" required>
                    </div>
                </div>`);
            keys++;
        });

        $(".removeKey").click(function () {
            $("#key" + $(this).attr('data-no')).remove();
        });
    });
</script>
@endpush
