@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    {{ __('translations.Carrier API Settings') }}
@endsection

@push('cssLib')
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Carrier API Settings') }}</a></li>
@endsection

@section('content')
<div class="page-content container-fluid">
    <div class="row text-right">
        <div class="col-12"><a href="{{ route('admin.carriers.create') }}" class="btn btn-primary">{{ __('translations.Add New Carrier') }}</a></div>
    </div>
    <div class="row mt-3">
        @foreach($carriers as $carrier)
            <div class="col-md-3">
                <div class="card">
                    <img style="width: 100%;" src="{{ $carrier->logo ? asset($carrier->logo) : 'https://picsum.photos/512/512' }}" class="card-img-top p-3">
                    <div class="card-body">
                        <h5 class="card-title">{{ $carrier->title }}</h5>
                        <form action="{{ route('admin.carriers.destroy', $carrier->id) }}" method="post">
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                            <input type="hidden" name="_method" value="DELETE">
                            <a href="{{ route('admin.carriers.edit', $carrier->id) }}" class="btn btn-primary">{{ __('translations.Edit Settings') }}</a>
                            <button class="btn btn-danger">{{ __('translations.Delete') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection

@push('scripts')
@endpush
