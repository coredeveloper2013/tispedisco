<nav class="sidebar-nav">
    <ul id="sidebarnav">
{{--        <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark" href="{{ route('admin.order.index') }}"--}}
{{--                                    aria-expanded="false"><i class="fas fa-compress"></i><span class="hide-menu">Home Settings</span></a>--}}
{{--            <ul aria-expanded="false" class="collapse first-level">--}}
{{--                <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-octagram"></i><span--}}
{{--                            class="hide-menu"> Slider</span></a></li>--}}
{{--            </ul>--}}
{{--        </li>--}}
        <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('admin.dashboard') }}" aria-expanded="false">
                <i class="mdi mdi-view-dashboard"></i><span class="hide-menu">{{ __('translations.Dashboard') }}</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('admin.order.index') }}" aria-expanded="false">
                <i class="far fa-lightbulb"></i><span class="hide-menu">{{ __('translations.Orders') }}</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark" href="{{ route('admin.users.index') }}" aria-expanded="false">
                <i class="fa fa-user"></i><span class="hide-menu">{{ __('translations.Users') }}</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark" href="{{ route('admin.tickets.index') }}" aria-expanded="false">
                <i class="mdi mdi-sync-alert"></i><span class="hide-menu">{{ __('translations.Tickets') }}</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark" href="{{ route('admin.pages.index') }}" aria-expanded="false">
                <i class="mdi mdi-sync-alert"></i><span class="hide-menu">{{ __('translations.Custom Pages') }}</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark" href="{{ route('admin.carriers.index') }}" aria-expanded="false">
                <i class="mdi mdi-settings"></i><span class="hide-menu">{{ __('translations.Carrier API Settings') }}</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark" href="{{ route('languages.index') }}" aria-expanded="false">
                <i class="mdi mdi-settings"></i><span class="hide-menu">{{ __('translations.Language Settings') }}</span>
            </a>
        </li>
    </ul>
</nav>
