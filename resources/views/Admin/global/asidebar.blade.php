<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        @include('Admin.global.menu')
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>