@extends('Admin.layouts.app')

@section('custom-css')
@endsection

@section('page-title')
    {{ __('translations.Tickets') }}
@endsection

@push('cssLib')
@endpush

@push('css')
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">{{ __('translations.Dashboard') }}</a></li>
@endsection

@section('content')
    <div class="page-content container-fluid" id="singleTicketVue">
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <h4 class="card-title">{{ __('translations.Ticket Title') }}: {{ $ticket->title }}</h4>
                        <h6 class="card-subtitle"></h6>
                        <div class="p-4">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">{{ __('translations.Ticket Messages') }}</h4>
                                    <div class="chat-box scrollable ps-container ps-theme-default"
                                         style="height:calc(100vh - 300px);"
                                         data-ps-id="72faadb9-75d0-7bbd-8952-0b2851bdd8b1">
                                        <!--chat Row -->
                                        <ul class="chat-list">
                                            <!--chat Row -->
                                            <section v-for="(reply, replyIndex) in showTicket.replies">
                                                <li class="chat-item">
                                                    <div class="chat-img">
                                                        <img src="{{ asset('template/assets/images/users/1.jpg') }}"
                                                            alt="user1">
                                                    </div>
                                                    <div class="chat-content">
                                                        <div class="box bg-light-success">
                                                            <h5 class="font-medium" v-if="reply.user_type == 'admin'">
                                                                @{{ reply.admin ? reply.admin.name : 'no name' }}
                                                            </h5>
                                                            <h5 class="font-medium" v-if="reply.user_type == 'user'">
                                                                @{{ reply.user.first_name+' '+reply.user.last_name
                                                                }}
                                                            </h5>
                                                            <p class="font-light mb-0">@{{reply.message}}</p>
                                                            <div class="chat-time">@{{moment(reply.created_at).calendar()}}</div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="odd chat-item" v-for="(innerReply, innerReplyIndex) in reply.replies">
                                                    <div class="chat-content">
                                                        <div class="box bg-light-success">
                                                            <h5 class="font-medium" v-if="innerReply.user_type == 'admin'">
                                                                @{{ innerReply.admin ? innerReply.admin.name : 'no name' }}
                                                            </h5>
                                                            <h5 class="font-medium" v-if="innerReply.user_type == 'user'">
                                                                @{{ innerReply.user.first_name+' '+innerReply.user.last_name }}
                                                            </h5>
                                                            <p class="font-light mb-0">@{{innerReply.message}}</p>
                                                            <div class="chat-time">@{{moment(innerReply.created_at).calendar()}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-img">
                                                        <img src="{{ asset('template/assets/images/users/2.jpg') }}"
                                                            alt="user2">
                                                    </div>
                                                </li>
                                            </section>
                                        </ul>
                                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                                            <div class="ps-scrollbar-x" tabindex="0"
                                                 style="left: 0px; width: 0px;"></div>
                                        </div>
                                        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                                            <div class="ps-scrollbar-y" tabindex="0"
                                                 style="top: 0px; height: 0px;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body border-top">
                                    <div class="row">
                                        <div class="col-9">
                                            <div class="input-field mt-0 mb-0">
                                                <input id="textarea1" placeholder="Type and enter"
                                                       class="form-control border-0" type="text" v-model="reply.message">
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <input type="file" id="replyFile" @change="handleFileUpload($event, 'reply')">
                                            <button class="btn-circle btn-lg btn-cyan float-right text-white" @click="replyCru(reply)"><i class="fas fa-paper-plane"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function Delete(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    axios.delete('{{url()->current()}}/' + id, {}).then(function (response) {
                        if (response.data.success) {
                            $('#tr-' + id).fadeOut();
                            Swal.fire(
                                'Deleted!',
                                'Your order has been deleted.',
                                'success'
                            )
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            });
        }

        function changeStatus(id, currentStatus) {
            axios.post('{{ route('admin.order.change-status') }}', {
                id: id,
                status: currentStatus
            }).then(function (response) {
                if (response.data.success) {
                    $('table').DataTable().ajax.reload(null, false);
                    toastr.success(response.data.message);
                } else {
                    toastr.error(response.data.message);
                }
            }).catch(function (error) {
                console.log(error);
                toastr.error(error.data.message);
            });
        }

        let fileUploadRoute = "{{route('ticket.fileUpload')}}";
        let getTicketsRoute = "{{route('user.getTickets')}}";
        let singleTicketRoute = "{{route('user.singleTicket')}}?ticketId=";
        let cruTicketRoute = "{{route('cru.ticket')}}";
        let cruReplyRoute = "{{route('cru.reply')}}";

        let ticketId = "{{ $ticket->id }}";
    </script>
    <script src="{{ asset('js/admin/pages/single-ticket.js') }}"></script>
@endpush
