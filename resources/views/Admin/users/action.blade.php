<a class="btn waves-effect waves-light btn-rounded btn-xs btn-info" href="{{ route('admin.users.show', $id) }}"><i
        class="fa fa-eye"></i></a>
<a class="btn waves-effect waves-light btn-rounded btn-xs btn-success" href="{{ route('admin.users.edit', $id) }}"><i
        class="fa fa-edit"></i></a>
@if ($status == 0)
    <button class="btn waves-effect waves-light btn-rounded btn-xs btn-danger" onclick="changeStatus({{$id}}, {{$status}})"><i class="mdi mdi-account-remove"></i></button>
@else
    <button class="btn waves-effect waves-light btn-rounded btn-xs btn-danger" onclick="changeStatus({{$id}}, {{$status}})"><i class="mdi mdi-account"></i></button>
@endif
