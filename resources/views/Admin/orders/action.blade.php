<button class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary" data-toggle="modal"
        data-target=".bs-example-modal-sm"
        onclick="vm.getSingleOrder({{$id}})">
    <i class="mdi mdi-clock"></i>
</button>
<a class="btn waves-effect waves-light btn-rounded btn-xs btn-info" href="{{ route('admin.order.show', $id) }}"><i
        class="fa fa-eye"></i></a>
{{--<a class="btn waves-effect waves-light btn-rounded btn-xs btn-success" href="{{ route('admin.order.edit', $id) }}"><i--}}
{{--        class="fa fa-edit"></i></a>--}}
<button class="btn waves-effect waves-light btn-rounded btn-xs btn-danger" onclick="Delete('{{$id}}')"><i
        class="fa fa-trash"></i></button>
